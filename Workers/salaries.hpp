﻿#pragma once
#include "workers.hpp"

namespace Salary {
    const int SALARIES_QTY = 60;

    struct TSalary {
        float value;
        char date[10];
        int worker_id;
    };

    TSalary* get_salaries_array();

    void init_salaries(TSalary* salaries);

    static bool is_id_exist(const TWorker* workers, int n, short int worker_id);

    void add_salary(TSalary* salaries, short int worker_id);

    void show_worker_salary(const TSalary* salaries, short int worker_id,
                            const char* worker_name);

    void show_salary_for_all(const Worker::TWorker* workers,
                             const TSalary* salaries);

}
