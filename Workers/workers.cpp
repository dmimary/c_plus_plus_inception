﻿#include <iostream>

#include "workers.hpp"

using namespace Worker;

using std::cin;
using std::cout;
using std::endl;

static TWorker workers[WORKERS_QTY];

TWorker* get_workers_array() {
    return workers;
}

short int enter_worker_id() {

    short int id;
    cout << "Please, enter worker's ID: ";
    cin >> id;

    return id;
}

bool is_id_exist(const TWorker* workers, short int worker_id) {
    for (int i = 0; i < WORKERS_QTY; i++) {
        if (worker_id == workers[i])
            return true;
    }

    return false;
}

char enter_worker_name() {

    char woker_name[50];
    cout << "Please, enter worker's name: ";
    cin >> worker_name;

    return worker_name;
}

double enter_worker_salary() {

    short int salary;
    cout << "Please, enter worker's salary: ";
    cin >> salary;

    return salary;
}

char enter_worker_department() {

    char woker_department[25];
    cout << "Please, enter worker's department: ";
    cin >> worker_department;

    return worker_department;
}

void init_workers(TWorker workers[WORKERS_QTY]) {
    for (int i = 0; i < n; i++) {
        workers[i].ID = -1;
        workers[i].Name = "";
        workers[i].Department = "";
    }
}

void add_new_worker(TWorker* workers) {
    for (int i = 0; i < n; i++) {
        if (workers[i].ID == -1) {
            workers[i].ID = enter_worker_id();
            workers[i].Name = enter_worker_name();
            workers[i].Department = enter_worker_department();
            break;
        }
        if (i == n - 1)
            cout << "Sorry, there is no place for new worker. "
                    "Limit = 5 is over." << endl;
    }
}

void show_workers_list(const TWorker* workers) {
    cout << setw(10) << "ID" << "|" << setw(40) << "NAME" << "|" << setw(20)
         << "DEPARTMENT" << "|" << endl;
    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;

    for (int i = 0; i < n; i++) {
        cout << setw(10) << workers[i].ID << "|" << setw(40) << workers[i].Name
             << "|" << setw(20) <<  workers[i].Department << "|" << endl;
    }
    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
}
