﻿#include <iostream>
#include <cstdlib>  // Include exit, EXIT_FAILURE

#include "user_interface.hpp"
#include "salaries.hpp"

using namespace Worker;
using namespace Salary;
using std::cin;
using std::cout;
using std::endl;

static unsigned short int ShowMenu() {
    int n = 0;

    cout << "1 - Add worker." << endl;
    cout << "2 - Show workers list." << endl;
    cout << "3 - Add salary for worker." << endl;
    cout << "4 - Show worker salary." << endl;
    cout << "5 - Show salary for all workers." << endl;
    cout << "6 - Exit." << endl;

    cin >> n;

    return n;
}

static short int enter_worker_id() {
    unsigned short int id = -1;

    cout << "Please, enter worker ID: ";
    cin >> id;

    return worker_id;
}

void show_interface() {
    init_workers(get_workers_array());
    init_salaries(init_salaries());

    unsigned int user_choise = 0;

    do {
        user_choise = ShowMenu();

        switch (user_choise) {
        case 1:
            add_new_worker(get_workers_array());
            break;

        case 2:
            show_workers_list(get_workers_array());
            break;

        case 3: {
            short int worker_id = enter_worker_id();
            if (is_id_exist(get_workers_array(), worker_id))
                    add_salary(get_salaries_array(), worker_id);
            else {
                cout << "Invalid ID!" << endl << "Given ID doesn't exist."
                     << endl << "Please, try again." << endl;
                exit(EXIT_FAILURE);
            }

            break;
        }

        case 4: {
            short int worker_id = enter_worker_id();
            if (is_id_exist(get_workers_array(), worker_id))
                    show_worker_salary(get_salaries_array(), worker_id,
                                       get_workers_array());
            else {
                cout << "Invalid ID!" << endl << "Given ID doesn't exist."
                     << endl << "Please, try again." << endl;
                exit(EXIT_FAILURE);
            }

            break;
        }
        case 5: show_salary_for_all(get_workers_array(), get_salaries_array());

        case 6: break;

        default:
            cout << "Wrong menu value! Please, try again." << endl;
        }
    } while(user_choise != 6);
}
