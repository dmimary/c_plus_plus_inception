﻿#pragma once

namespace Worker {

    const int WORKERS_QTY = 5;

    struct TWorker {
        int ID;
        char name[50];
        char department[25];
    };

    TWorker* get_workers_array();

    short int enter_worker_id();

    bool is_id_exist(const TWorker* workers, short int worker_id);

    char enter_worker_name();

    double enter_worker_salary();

    char enter_worker_department();

    void init_workers(TWorker* workers);

    void add_new_worker(TWorker* workers);

    void show_workers_list(const TWorker* workers);
}
