﻿#include "gas_station.hpp"

#include <iostream>  // cout, endl
#include <fstream>  // std::ifstream
#include <cstddef>  // std::size_t
#include <cstdlib>  // std::exit
#include <iomanip>  // std::setw

using std::endl;
using std::cout;
using std::size_t;
using std::setw;

void display(queue_t* front) {
    size_t str_num = 0;

    if (front == NULL) {
        cout << "There is no one to this fuel dispanser." << endl;
    } else {
        cout << "------------------------------------------------------" << endl
         << "#" << setw(19) << "vehicle brand | " << setw(16)
         << "number plate | " << setw(9) << "fuel type" << endl
         << "------------------------------------------------------" << endl;

        while (front != NULL) {
            str_num++;

            cout << str_num << ". " << setw(14) << front->order.brand << " | "
                 << setw(10) << front->order.number << " | "
                 << setw(9) << front->order.fuel_type << endl;

            front = front->next;
        }
    }
}

order_t dequeue(queue_t** front) {
    order_t temp = (*front)->order;  // Save data for return.
    // Save the address of the queue beginning don't lose it and delete later:
    queue_t* new_front = *front;
    // Rearrange the pointer to the beginning to the previous elememt:
    *front = (*front)->next;
    delete new_front;  // Free up memory.

    return temp;
}

void dequeue_all(queue_t** front) {
    order_t order;

    while (*front != NULL) {
        order = dequeue(front);
        cout << "× Order " << order.brand << " " << order.number << " "
             << order.fuel_type << " is gone..." << endl;
    }

}

queue_t* create_common_queue() {
    std::ifstream file("orders.txt");  // open file with orders

    // Check if file is located in current directory and not empty.
    if (!file || !file.is_open()) {
        std::cerr << "File is empty or doesn't exist in current directory!" << endl;
        std::exit(EXIT_FAILURE);
    }

    // allocate memory:
    queue_t* front = new queue_t;
    queue_t* back = NULL;

    for (string line; std::getline(file, line); ) {

        if (back == NULL) {
            front->next = NULL;
            back = front;
        } else {
            back->next = new queue_t;
            back = back->next;
        }

        size_t found_fst = line.find_first_of(',');  // index of first ','
        size_t found_snd = line.find_last_of(',');  // index of last ','

        back->order.brand = line.substr(0 , found_fst);
        back->order.number = line.substr(line.find(',') + 1,
                                              found_snd - found_fst - 1 );
        back->order.fuel_type = line.substr(found_snd+1);
        back->next = NULL;
    }

    file.close();

    return front;
}

static const queue_t* find(const queue_t* p_currect, const string key) {
    while ((p_currect != NULL) && (p_currect->order.fuel_type != key)) {
        p_currect = p_currect->next;
}
    return p_currect;
}

queue_t* create_queue_by_fuel(const queue_t* common_queue, const string octain_num) {

    const queue_t* found = find(common_queue, octain_num);
    queue_t* front = NULL;

    if (found != NULL) {
        front = new queue_t;
        queue_t* back = NULL;

        while (found != NULL) {

            if (back == NULL) {
                front->next = NULL;
                back = front;
            } else {
                back->next = new queue_t;  // back->next is like a new element
                back = back->next;
            }

                back->order.brand = found->order.brand;
                back->order.number = found->order.number;
                back->order.fuel_type = found->order.fuel_type;

                back->next = NULL;

                found = find(found->next, octain_num);
            }

        }

    return front;
}
