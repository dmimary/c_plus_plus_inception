﻿#include "gas_station.hpp"

#include <iostream>  // cout, endl
#include <iomanip>  // setw

using std::cout;
using std::endl;
using std::setw;


void show() {
    // another pointer to first elem in common queue:
    queue_t* common_queue = create_common_queue();
    // another pointer to first elem in 92nd fuel queue:
    queue_t* queue_92 = create_queue_by_fuel(common_queue, "92");
    // another pointer to first elem in 95nd fuel queue:
    queue_t* queue_95 = create_queue_by_fuel(common_queue, "95");
    // another pointer to first elem in 98nd fuel queue:
    queue_t* queue_98 = create_queue_by_fuel(common_queue, "98");
    // another pointer to first elem in diesel fuel queue:
    queue_t* queue_DF = create_queue_by_fuel(common_queue, "DF");

    cout << "  ***" << setw(30) << "Common queue of all orders:" << endl;
    display(common_queue);
    cout << endl << endl;

    cout << "  ***" << setw(45) << "A queue of orders to 92nd fuel dispenser:"
         << endl;
    display(queue_92);
    cout << endl << endl;

    cout << "  ***" << setw(45) << "A queue of orders to 95th fuel dispenser:"
         << endl;
    display(queue_95);
    cout << endl << endl;

    cout << "  ***" << setw(45) << "A queue of orders to 98th fuel dispenser:"
         << endl;
    display(queue_98);
    cout << endl << endl;

    cout << "  ***" << setw(45) << "A queue of orders to diesel fuel dispenser:"
         << endl;
    display(queue_DF);

    cout << endl << endl << "Common queue is unnecessary any more. RIP."
         << endl << endl;

    dequeue_all(&common_queue);
    cout << endl << "Vehicles in the queue to 92nd fuel dispenser are fueled:"
         << endl << endl;
    dequeue_all(&queue_92);

    cout << endl << "Vehicles in the queue to 95th fuel dispenser are fueled:"
         << endl << endl;
    dequeue_all(&queue_95);

    cout << endl << "Vehicles in the queue to 98th fuel dispenser are fueled:"
         << endl << endl;
    dequeue_all(&queue_98);

    cout << endl << "Vehicles in the queue to diesel fuel dispenser are fueled:"
         << endl << endl;
    dequeue_all(&queue_DF);
    cout << endl;

}
