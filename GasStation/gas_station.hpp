﻿#pragma once

#include <string>  // std::string

using std::string;

//! Structure of refueling order.
struct order_t {
    string brand;  // e.g. Ferrari
    string number;  // e.g. A789BC + '\0'
    string fuel_type;  // can be 92, 95, 98 or DF(Diesel Fuel) + '\0'
};

//! Structure of order for common queue, 92nd, 95th, 98th gasoline and diesel ones.
struct queue_t {
    order_t order;
    queue_t* next;

};

//! Display queue - brand, number and fuel type of each vehicle.
void display(queue_t* front);

//! Remove an item (vehicle) from the queue.
order_t dequeue(queue_t** front);

//! Remove all items (vehicles) from the queue.
void dequeue_all(queue_t** front);

//! Create a queue of orders taken from text file.
queue_t* create_common_queue();

//! Find an element in common queue by fuel type(92nd, 95th, 98th or diesel)
static const queue_t* find(const queue_t* p_currect, const string key);

//! Create a queue by fuel type (octain number) from common queue.
queue_t* create_queue_by_fuel(const queue_t* common_queue, const string octain_num);

void show();
