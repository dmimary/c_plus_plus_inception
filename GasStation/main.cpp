﻿/*
* Exapmle of queue implementation. Reality-like queues at the gas station
* where are 4 fuel types -> 4 fuel dispensers (fluel machines).
* There is one common queue of all orders in a mess.
* Reading orders from the text file orders.txt. and distribution to 4 queues
* by fuel type (92, 95, 98, diesel). Removing elements from the beginning
* to the end of the queue one by one or all at once.
*/

#include "gas_station.hpp"

int main()
{
    show();

    return 0;
}
