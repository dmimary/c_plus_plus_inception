﻿/*!
  Set of different examples to study WHILE, DO-WHILE & FOR loops.

  @author Maria Dmitrieva
*/

#include <iostream>
#include <iomanip>    // setprecision()
#include <cassert>

#include "loop-s.hpp"

using std::cout;
using std::endl;

//! Note! All trigonometric functions take as an argument the value in degrees!

int main()
{
    cout << "!Note! All trigonometric functions take as an argument the value "
            "in degrees!\n\n";

    //================================#1.1==========================================

    // Test SumOddsWhile func. Task 1.1.
    assert(SumOddsWhile() == 625);

    // Test SumOddsDoWhile func. Task 1.1.
    assert(SumOddsDoWhile() == 625);

    // Test SumOddsFor func. Task 1.1.
    assert(SumOddsFor() == 625);

    // Output result of SumOddsFor func for e.g.
    cout << "Task 1.1. Tests are passed.\n"
            "Sum of the odd numbers in range [1,50] = " << SumOddsFor()
         << endl << endl;

//================================#1.2==========================================
    // Task 1.2. Tests Sum20th while, do-while and for functions.
    // NOTE! The number 100 was deliberatly changed to 20 in order to avoid
    // the unsigned long long overflow.
    assert(Sum20thWhile() == Sum20thDoWhile() &&
           Sum20thWhile() ==  Sum20thFor());

    // Output result of Sum20thFor func for e.g.
    cout << "Task 1.2. \n"
            "The 20th partial sum of series 1/i!^2, where i = [1,20]\n"
            "approximately equals ≈ ";
    cout << std::setprecision(10) << Sum20thFor() << endl << endl;

//================================#1.3==========================================

    // Task 1.3. Output result of SinXinRangeFor func for e.g.
    cout << std::setprecision(5) << "Task 1.3. \n"
            "sin(x) in range [-10;10] by step h = 0.5:\n" << endl;
    SinXinRangeFor();
    cout << endl;

//================================#8.1==========================================

    // Task 8.1. Tests SumSinx while, do-while and for functions.
    assert(SumSinxWhile(3, 1) == SumSinxDoWhile(3, 1) &&
           SumSinxWhile(3, 1) == SumSinxFor(3, 1));

    // Output result of SumSinx while, do-while and for functions.
    cout << "Task 8.1. \n"
            "The n-th partial sum: sin x + sin^2 x +...+ sin^n x :\n";

    cout << "1) sin " << 0 << " = " << SumSinxWhile(1, 0.0) << endl;

    cout << "2) sin " << 180 << " + " << "sin^2 " << 180 << " ≈ "
         << SumSinxFor(2, 180) << endl;

    cout << "2) sin " << 45 << " + " << "sin^2 " << 45 << " sin^3 " << 45 << " ≈ "
         << SumSinxFor(3, 45) << endl << endl;

//================================#8.2==========================================

    // Task 8.2. Tests Product10th while, do-while and for functions.
    assert(Product10thWhile() == Product10thDoWhile() &&
           Product10thDoWhile() == Product10thFor());

    // Output result of Sum20thFor func for e.g.
    cout << "Task 8.2. \n"
            "The 10th partial product : (1 - 1/i!)^2, where i = [2,10]\n"
            "approximately equals ≈ "
         << Product10thFor() << endl << endl;

//================================#8.3==========================================

    // Task 8.3. Output result of Sin3XinRangeFor func for e.g.
    cout << "Task 8.3. \n"
            "sin(3x) in range [-4;4] by step h = 0.1:\n" << endl;
    Sin3XinRangeFor();
    cout << endl;

//================================#14.1==========================================

    // Task 14.1. Tests ProductOdds funcs while, do-while and for functions.
    assert(ProductOddsWhile() == 12604484198222791875u &&
           ProductOddsWhile() == ProductOddsDoWhile() &&
           ProductOddsDoWhile() ==  ProductOddsFor());

    // Output result of ProductOddsFor func for e.g.
    cout << "Task 14.1. Tests are passed.\n"
            "The product of the odd numbers multiplied by 3 in range [1,25] =\n"
         << "= " << ProductOddsFor() << endl << endl;

//================================#14.2=========================================

    // Task 14.2. Tests ProductNth while, do-while and for functions.
    assert(ProductNthWhile(0.0, 1) == ProductNthDoWhile(0.0, 1) &&
           ProductNthDoWhile(0.0, 1) == ProductNthFor(0.0, 1));
    cout << "Task 14.2. Tests are passed.\n";

    // Output result of ProductNth while, do-while and for functions.
    cout << "1) The partial product: 1 + (sin(kx) / k!), where k = [1,20],"
            "x = 0.0, equals "
         << ProductNthWhile(0.0, 20) << endl << endl;
    cout << "2) The partial product: 1 + (sin(kx) / k!), where k = [1,3],"
            "x = -90.0, equals "
         << ProductNthDoWhile(-90.0, 3) << endl << endl;
    cout << "3) The partial product: 1 + (sin(kx) / k!), where where k = [1,10],"
            " x = 45.0,\n   approximately equals ≈ "
         << ProductNthFor(45.0, 10) << endl << endl;

//================================#14.3=========================================

    // Task 14.3. Output result of TanXinRange func for e..
    cout << "Task 14.3. \n"
            "tg(x)/x^2 in range [-5;5] by step h = 0.1:\n" << endl;
    TanXinRangeFor();
    cout << endl;

//================================#15.1=========================================

    // Task 15.1. Tests SumSqrts while, do-while and for functions.
    assert(SumSqrtsWhile() == SumSqrtsDoWhile() &&
           SumSqrtsDoWhile() == SumSqrtsFor());

    // Output result of SumSqrtsFor func for e.g.
    cout << "Task 15.1. Tests are passed.\n"
            "The sum of the square roots of even numbers in range [1;50] "
            "approximately equals\n≈ "
         << SumSqrtsFor() << endl << endl;

//================================#15.2=========================================

    // Task 15.1. Output result of PartialSumNth while, do-while and for func-s.
    assert(PartialSumNthWhile(1, 1.0) == PartialSumNthDoWhile(1, 1.0) &&
           PartialSumNthDoWhile(1, 1.0) == PartialSumNthFor(1, 1.0));

    // Output result of SumSqrts while, do-while and for functions.
    cout << "Task 15.2. Tests are passed.\n";

    cout << "1) The n-th partial sum of series: (1/i! + sqrt(|x|), "
            "where i = [1,1], x = 1.0,\n   equals = "
         << PartialSumNthWhile(1, 1.0) << endl;
    cout << "2) The n-th partial sum of series: (1/i! + sqrt(|x|), "
            "where i = [1,3], x = 4.0,\n   approximately equals ≈ "
         << PartialSumNthDoWhile(3, 4.0) << endl;
    cout << "3) The n-th partial sum of series: (1/i! + sqrt(|x|), "
            "where i = [1,20], x = -1.0\n   approximately equals ≈ "
         << PartialSumNthFor(20, -1.0) << endl << endl;

//================================#15.3=========================================

    // Task 15.3. Output result of TanXinRangeFor func for e.g.
    cout << "Task 15.3. \n"
            "tg(x)/(x + 2)^2 in range [-5,5] by step h = 0.1:\n" << endl;
    TangentXinRangeFor();
    cout << endl;

//================================#17.1=========================================

    // Task 17.1. Tests SumNegatives while, do-while and for functions.
    assert(SumNegativesWhile() == SumNegativesDoWhile() &&
           SumNegativesDoWhile() == SumNegativesFor());

    // Output result of SumSqrtsFor func for e.g.
    cout << "Task 17.1. Tests are passed.\n"
            "The sum of the negative odd numbers in range [-20,20] equals = "
         << SumNegativesFor() << endl << endl;

//================================#17.2=========================================

    // Task 17.1. Tests PartNthSum while, do-while and for functions.
    assert(PartNthSumWhile(0) == PartNthSumDoWhile(0) &&
           PartNthSumDoWhile(0) == PartNthSumFor(0));

    // Output result of PartNthSum while, do-while and for functions.
    cout << "Task 17.2. Tests are passed.\n";

    cout << "1) The n-th partial sum of series: (-1)^k*(k+1) / k!, "
            "where k = [0,0], \n   equals = "
         << PartNthSumWhile(0) << endl;
    cout << "2) The n-th partial sum of series: (-1)^k*(k+1) / k!, "
            "where k = [0,1], \n   equals = "
         << PartNthSumDoWhile(1) << endl;
    cout << "3) The n-th partial sum of series: (-1)^k*(k+1) / k!, "
            "where k = [0,20], \n   approximately equals ≈ "
         << PartNthSumFor(20) << endl << endl;

//================================#17.3=========================================

    // Task 17.3. Output result of ExpXinRangeFor func for e.g.
    cout << "Task 17.3. \n"
            "4e^x / x^2 in range [-7,7] by step h = 0.3:\n" << endl;
    ExpXinRangeWhile();
    cout << endl;

    cout << "P.S. There are such functions as sin(0), tan(0) don't equal 0.0\n"
            "because of the precision.\n\n"
            "===============================THE END============================"
            "==============\n" << endl;

}
