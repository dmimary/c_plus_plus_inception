﻿#include <iostream>
#include <cmath>

#include "loop-s.hpp"

using std::cout;
using std::endl;
using std::pow;
using std::sin;
using std::tan;
using std::sqrt;
using std::abs;
using std::exp;

// Computation the Factorial of n to reusable use in other functions.
// NOTE! // Unsigned long long can stor max 20! otherwise there will overflow.
unsigned long long factorial(unsigned int n) {
    unsigned long long result = 1;
    if (n > 20) return 0;  // To show that value of n is bigger than possible.
    else
        for (int i = 2; i <= n; i++) {
                result *= i;
        }

    return result;
}

// Display a warning message about unsigned long loong overflow
void warning() {
    cout << "Oops! Unsigned long long can stor max 20! otherwise there will "
            "overflow and the result\nwill be incorrect. "
            "So, there is the max result for n = 20." << endl;
}

//================================#1.1==========================================
// Calculate a sum of the odd numbers in range [1,50].

// WHILE
int SumOddsWhile() {
    int sum = 0;
    int i = 1;

    while (i <= 50) {
        if (i % 2 != 0)    // Check wether i is odd or not
            sum += i;
        ++i;
    }

    return sum;
}

// DO-WHILE
int SumOddsDoWhile() {
    int sum = 0;
    int i = 1;

    do {
        if (i % 2 != 0)    // Check wether i is odd or not
            sum += i;
        ++i;
    } while (i <= 50);

    return sum;
}

// FOR
int SumOddsFor() {
    int sum = 0;

    for (int i = 1; i <= 50; i++) {
        if (i % 2 != 0)    // Check wether i is odd or not
            sum += i;
    }

    return sum;
}

//================================#1.2==========================================
// Calculate the 20th partial sum of series 1/i!^2.
// NOTE! The number 100 was deliberatly changed to 20 in order to avoid
// the unsigned long long overflow.

// WHILE
long double Sum20thWhile() {
    long double sum = 0.0l;
    int i = 1;

    while (i <= 20) {
            sum += 1.0l / pow(factorial(i), 2);
            ++i;
    }

    return sum;
}

// DO-WHILE
long double Sum20thDoWhile() {
    long double sum = 0.0l;
    int i = 1;

    do {
        sum += 1.0l / pow(factorial(i), 2);
        ++i;
    } while (i <= 20);

    return sum;
}

// FOR
long double Sum20thFor() {
    long double sum = 0.0l;

    for (int i = 1; i <= 20; i++) {
        sum += 1.0l / pow(factorial(i), 2);
    }

    return sum;
}

//================================#1.3==========================================
// Calculate sin(x) in range [-10;10] by step h = 0.5.
// NOTE! x * M_PI / 180.0 - to express in radians.
//       One radian is equivalent to 180/PI degrees.

// WHILE
void SinXinRangeWhile() {
    float x = -10.0;

    while (x <= 10.0) {
        cout << "sin(" << x << ") ≈ " << sin(x * M_PI / 180.0) << endl;
        x += 0.5;
    }
}

// DO-WHILE
void SinXinRangeDoWhile() {
    float x = -10.0;

    do {
        cout << "sin(" << x << ") ≈ " << sin(x * M_PI / 180.0) << endl;
        x += 0.5;
    } while (x <= 10.0);
}

// FOR
void SinXinRangeFor() {

    for (float x = -10.0; x <= 10.0; x += 0.5) {
        cout << "sin(" << x << ") ≈ " << sin(x * M_PI / 180.0) << endl;
    }
}

//================================#8.1==========================================
// Calculate n-th partial sum: sin x + sin^2 x +...+ sin^n x
// NOTE! x * M_PI / 180.0 - to express in radians

// WHILE
double SumSinxWhile(int n, float x) {
    double sum = 0.0;
    int i = 1;

    while (i <= n) {
        sum += pow(sin(x * M_PI / 180.0), i);
        ++i;
    }

    return sum;
}

// DO-WHILE
double SumSinxDoWhile(int n, float x) {
    double sum = 0.0;
    int i = 1;

    do {
        sum += pow(sin(x * M_PI / 180.0), i);
        ++i;
    } while (i <= n);

    return sum;
}

// FOR
double SumSinxFor(int n, float x) {
    double sum = 0.0;

    for (int i = 1; i <= n; i++) {
        sum += pow(sin(x * M_PI / 180.0), i);
    }

    return sum;
}

//================================#8.2==========================================
// Calculate the 10th partial product: (1 - 1/i!)^2

// WHILE
double Product10thWhile() {
    double product = 1.0;
    int i = 2;

    while (i <= 10) {
        product *= pow(1.0 - (1.0 / factorial(i)), 2.0);
        ++i;
    }

    return product;
}

// DO-WHILE
double Product10thDoWhile() {
    double product = 1.0;
    int i = 2;

    do {
        product *= pow(1.0 - (1.0 / factorial(i)), 2.0);
        ++i;
    } while (i <= 10);

    return product;
}

// FOR
double Product10thFor() {
    double product = 1.0;

    for (int i = 2; i <= 10; i++) {
        product *= pow(1.0 - (1.0 / factorial(i)), 2.0);
    }

    return product;
}

//================================#8.3==========================================
// Calculate sin(3x) in range [-4;4] by step h = 0.1.
// NOTE! (3 * x) * M_PI / 180.0 - to express in radians

// WHILE
void Sin3XinRangeWhile() {
    double x = -4.0;

    while (x <= 4.0) {
        cout << "sin(" << 3 * x << ") ≈ " << sin((3 * x) * M_PI / 180.0) << endl;
        x += 0.1;
    }
}

// DO-WHILE
void Sin3XinRangeDoWhile() {
    double x = -4.0;

    do {
        cout << "sin(" << 3 * x << ") ≈ " << sin((3 * x) * M_PI / 180.0) << endl;
        x += 0.1;
    } while (x <= 4.0);
}

// FOR
void Sin3XinRangeFor() {

    for (double x = -4.0; x <= 4.0; x += 0.1) {
        cout << "sin(" << 3 * x << ") ≈ " << sin((3 * x) * M_PI / 180.0) << endl;
    }
}

//================================#14.1=========================================
// Calculates the product of the odd numbers multiplied by 3 in range [1;25]
// NOTE! The right boundary 50 was deliberatly changed to 25 in order to avoid
// the unsigned long long overflow!

// WHILE
unsigned long long ProductOddsWhile() {
    unsigned long long product = 1;
    int i = 1;

    while (i <= 25) {
        if (i % 2 != 0)  // Check wether i is odd or not
            product *= i * 3ull;
        ++i;
    }

    return product;
}

// DO-WHILE
unsigned long long ProductOddsDoWhile() {
    unsigned long long product = 1;
    int i = 1;

    do {
        if (i % 2 != 0)  // Check wether i is odd or not
            product *= i * 3ll;
        ++i;
    } while (i <= 25);

    return product;
}

// FOR
unsigned long long ProductOddsFor() {
    unsigned long long product = 1;
    int i = 1;

    for (int i = 1; i <= 25; i++) {
        if (i % 2 != 0)  // Check wether i is odd or not
            product *= i * 3ll;
    }

    return product;
}

//================================#14.2=========================================
// Calculate the partial product: 1 + (sin(kx) / k!)
// NOTE! (k * M_PI / 180) * x - to express in radians

// WHILE
long double ProductNthWhile(float x, int n) {
    long double product = 1.0l;
    int k = 1;

    while (k <= n) {
        if (factorial(k) == 0) {
            warning();  // message about an overflow
            break;
        }
        product *= 1.0l + sin(k * (x * M_PI / 180.0)) / factorial(k);
        ++k;
    }

    return product;
}

// DO-WHILE
long double ProductNthDoWhile(float x, int n) {
    long double product = 1.0l;
    int k = 1;

    do {
        if (factorial(k) == 0) {
            warning();  // message about an overflow
            break;
        }
        product *= 1.0l + sin(k * (x * M_PI / 180.0)) / factorial(k);
        ++k;
    } while (k <= n);

    return product;
}

// FOR
long double ProductNthFor(float x, int n) {
    long double product = 1.0l;

    for (int k = 1; k <= n; k++) {
        if (factorial(k) == 0) {
            warning();  // message about an overflow
            break;
        }
        product *= 1.0l + sin(k * (x * M_PI / 180.0) / factorial(k));
    }

    return product;
}

//================================#14.3=========================================
// Calculate tg(x)/x^2 in range [-5;5] by step h = 0.1.

// WHILE
void TanXinRangeWhile() {
    double x = -5.0;

    while (x <= 5.0) {
        if (x != 0.0)
            cout << "tan(" << x << ")/" << x << "^2 ≈ "
                 << tan(x * M_PI / 180.0) / pow(x, 2.0) << endl;
        else cout << "-" << endl;
        x += 0.1;
    }
}

// DO-WHILE
void TanXinRangeDoWhile() {
    double x = -5.0;

    do {
        if (x != 0.0)
            cout << "tan(" << x << ")/" << x << "^2 ≈ "
                 << tan(x * M_PI / 180.0) / pow(x, 2.0) << endl;
        else cout << "-" << endl;
        x += 0.1;
    } while (x <= 5.0);
}

// FOR
void TanXinRangeFor() {

    for (double x = -5.0; x <= 5.0; x += 0.1) {
        if (x != 0.0)
            cout << "tan(" << x << ")/" << x << "^2 ≈ "
                 << tan(x * M_PI / 180.0) / pow(x, 2.0) << endl;
        else cout << "-" << endl;
    }
}

//================================#15.1=========================================
// Calculate the sum of the square roots of even numbers in range [1;50]

// WHILE
double SumSqrtsWhile() {
    int i = 1;
    double sum = 0.0;

    while (i <= 50) {
        if (i % 2 == 0)
            sum += sqrt(static_cast<double>(i));
        ++i;
    }

    return sum;
}

// DO-WHILE
double SumSqrtsDoWhile() {
    int i = 1;
    double sum = 0.0;

     do {
        if (i % 2 == 0)
            sum += sqrt(static_cast<double>(i));
        ++i;
    } while (i <= 50);

    return sum;
}

// FOR
double SumSqrtsFor() {
    double sum = 0.0;

    for (int i = 1; i <= 50; i++) {
        if (i % 2 == 0)
            sum += sqrt(static_cast<double>(i));
    }

    return sum;
}

//================================#15.2=========================================
// Calculate a n-th partial sum of series: (1/i! + sqrt(|x|)

// WHILE
double PartialSumNthWhile(int n, float x) {
    double sum = 0.0;
    int i = 1;

    while (i <= n) {
        if (factorial(i) == 0) {
            warning();  // message about an overflow
            break;
        }
        sum += 1.0 / (factorial(i)) + sqrt(abs(x));
        ++i;
    }

    return sum;
}

// DO-WHILE
double PartialSumNthDoWhile(int n, float x) {
    double sum = 0.0;
    int i = 1;

    do {
        if (factorial(i) == 0) {
            warning();  // message about an overflow
            break;
        }
        sum += 1 / static_cast<double>(factorial(i)) + sqrt(abs(x));
        ++i;
    } while (i <= n);

    return sum;
}

// FOR
double PartialSumNthFor(int n, float x) {
    double sum = 0.0;

    for (int i = 1; i <= n; i++) {
        if (factorial(i) == 0) {
            warning();  // message about an overflow
            break;
        }
        sum += 1 / static_cast<double>(factorial(i)) + sqrt(abs(x));
    }

    return sum;
}

//================================#15.3=========================================
// Calculate tg(x)/(x + 2)^2 in range [-5;5] by step h = 0.1.

// WHILE
void TangentXinRangeWhile() {
    double x = -5.0;

    while (x <= 5.0) {
        if (x != 0.0)
            cout << "tan(" << x << ")/(" << x << " + 2)^2 ≈ "
                 << tan(x * M_PI / 180.0) / pow(x + 2.0, 2.0) << endl;
        else cout << "-" << endl;
        x += 0.1;
    }
}

// DO-WHILE
void TangentXinRangeDoWhile() {
    double x = -5.0;

    do {
        if (x != 0.0)
            cout << "tan(" << x << ")/(" << x << " + 2)^2 ≈ "
                 << tan(x * M_PI / 180.0) / pow(x + 2.0, 2.0) << endl;
        else cout << "-" << endl;
        x += 0.1;
    } while (x <= 5.0);
}

// FOR
void TangentXinRangeFor() {
    for (double x = -5.0; x <= 5.0; x += 0.1) {
        if (x != 0.0)
            cout << "tan(" << x << ")/(" << x << " + 2)^2 ≈ "
                 << tan(x * M_PI / 180.0) / pow(x + 2.0, 2.0) << endl;
        else cout << "-" << endl;
    }
}

//================================#17.1=========================================
// Calculate the sum of the negative odd numbers in range [-20;20]

// WHILE
int SumNegativesWhile() {
    int i = -20;
    int sum = 0;

    while (i < 0) {
        if (i % 2 != 0)
            sum += i;
        ++i;
    }

    return sum;
}

// DO-WHILE
int SumNegativesDoWhile() {
    int i = -20;
    int sum = 0;

    do {
        if (i % 2 != 0)
            sum += i;
        ++i;
    } while (i < 0);

    return sum;
}

// FOR
int SumNegativesFor() {
    int sum = 0;

    for (int i = -20; i < 0; i++) {
        if (i % 2 != 0)
            sum += i;
    }

    return sum;
}

//================================#17.2=========================================
// Calculate a n-th partial sum of series: (-1)^k*(k+1) / k!

// WHILE
double PartNthSumWhile(int n) {
    double sum = 0.0;
    int k = 0;

    while (k <= n) {
        if (factorial(k) == 0) {
            warning();  // message about an overflow
            break;
        }
        sum += (pow(-1.0, k) * (k + 1.0)) / static_cast<double>(factorial(k));
        ++k;
    }

    return sum;
}

// DO-WHILE
double PartNthSumDoWhile(int n) {
    double sum = 0.0;
    int k = 0;
    int fact_k = 1;

    do {
        if (factorial(k) == 0) {
            warning();  // message about an overflow
            break;
        }
        sum += (pow(-1.0, k) * (k + 1.0)) / static_cast<double>(factorial(k));
        ++k;
    } while (k <= n);

    return sum;
}

// FOR
double PartNthSumFor(int n) {
    double sum = 0.0;

    for (int k = 0; k <= n; k++) {
        if (factorial(k) == 0) {
            warning();  // message about an overflow
            break;
        }
        sum += (pow(-1.0, k) * (k + 1.0)) / static_cast<double>(factorial(k));
    }

    return sum;
}

//================================#17.3=========================================
// Calculates 4e^x / x^2 in range [-7;7] by step h = 0.3.

// WHILE
void ExpXinRangeWhile() {
    double x = -7.0;

    while (x <= 7.0) {
        if (x != 0.0)
            cout << "4 * exp(" << x << ") ) / pow(" << x << ", 2) ≈ "
                 << (4.0 * exp(x)) / pow(x, 2.0) << endl;

        else cout << "-" << endl;
        x += 0.3;
    }
}

// DO-WHILE
void ExpXinRangeDoWhile() {
    double x = -7.0;

    do {
        if (x != 0.0)
            cout << (4.0 * exp(x)) / pow(x, 2.0) << endl;
        else cout << "-" << endl;
        x += 0.3;
    } while (x <= 7.0);
}

// FOR
void ExpXinRangeFor() {
    for (double x = -7.0; x <= 7.0; x += 0.3) {
        if (x != 0.0)
            cout << (4.0 * exp(x)) / pow(x, 2.0) << endl;
        else cout << "-" << endl;
    }
}
