﻿#pragma once

#ifndef M_PI
#define M_PI 3.14159265
#endif

//! Computation the Factorial of n to reusable use in other functions
unsigned long long factorial(unsigned int n);

//! Display a warning message about unsigned long loong overflow
void warning();

//================================#1.1==========================================

//! H/w 1. Loops, task #1.1. WHILE
//! Calculate a sum of the odd numbers in range [1,50]
int SumOddsWhile();

//! H/w 1. Loops, task #1.1. DO-WHILE
//! Calculate a sum of the odd numbers in range [1,50]
int SumOddsDoWhile();

//! H/w 1. Loops, task #1.1. FOR
//! Calculate a sum of the odd numbers in range [1,50]
int SumOddsFor();

//================================#1.2==========================================
// The task was changed (100 -> 20) in order to avoid the factorial overflow
//! H/w 1. Loops, task #1.2. WHILE
//! Calculate the 20th partial sum of series 1/i!^2
long double Sum20thWhile();

//! H/w 1. Loops, task #1.2. DO-WHILE
//! Calculate the 20th partial sum of series 1/i!^2
long double Sum20thDoWhile();

//! H/w 1. Loops, task #1.2. FOR
//! Calculate the 20th partial sum of series 1/i!^2
long double Sum20thFor();


//================================#1.3==========================================

//! H/w 1. Loops, task #1.3. WHILE
//! Calculate sin(x) in range [-10;10] by step h = 0.5.
void SinXinRangeWhile();

//! H/w 1. Loops, task #1.3. DO-WHILE
//! Calculate sin(x) in range [-10;10] by step h = 0.5.
void SinXinRangeDoWhile();

//! H/w 1. Loops, task #1.3. FOR
//! Calculate sin(x) in range [-10;10] by step h = 0.5.
void SinXinRangeFor();

//================================#8.1==========================================

//! H/w 1. Loops, task #8.1. WHILE
//! Calculate n-th partial sum: sin x + sin^2 x +...+ sin^n x
double SumSinxWhile(int n, float x);

//! H/w 1. Loops, task #8.1. DO-WHILE
//! Calculate n-th partial sum: sin x + sin^2 x +...+ sin^n x
double SumSinxDoWhile(int n, float x);

//! H/w 1. Loops, task #8.1. FOR
//! Calculate n-th partial sum: sin x + sin^2 x +...+ sin^n x
double SumSinxFor(int n, float x);

//================================#8.2==========================================

//! H/w 1. Loops, task #8.2. WHILE
//! Calculate the 10th partial product: (1 - 1/i!)^2
double Product10thWhile();

//! H/w 1. Loops, task #8.2. DO-WHILE
//! Calculate the 10th partial product: (1 - 1/i!)^2
double Product10thDoWhile();

//! H/w 1. Loops, task #8.2. FOR
//! Calculate the 10th partial product: (1 - 1/i!)^2
double Product10thFor();

//================================#8.3==========================================

//! H/w 1. Loops, task #8.3. WHILE
//! Calculate sin(3x) in range [-4;4] by step h = 0.1.
void Sin3XinRangeWhile();

//! H/w 1. Loops, task #8.3. DO-WHILE
//! Calculate sin(3x) in range [-4;4] by step h = 0.1.
void Sin3XinRangeDoWhile();

//! H/w 1. Loops, task #8.3. FOR
//! Calculate sin(3x) in range [-4;4] by step h = 0.1.
void Sin3XinRangeFor();

//================================#14.1=========================================
//! The right boundary 50 was deliberatly changed to 25 in order to avoid
//! the unsigned long long overflow!

//! H/w 1. Loops, task #14.1. WHILE
//! Calculates the product of the odd numbers multiplied by 3 in range [1;25]
unsigned long long ProductOddsWhile();

//! H/w 1. Loops, task #14.1. DO-WHILE
//! Calculates the product of the odd numbers multiplied by 3 in range [1;25]
unsigned long long ProductOddsDoWhile();

//! H/w 1. Loops, task #14.1. FOR
//! Calculates the product of the odd numbers multiplied by 3 in range [1;25]
unsigned long long ProductOddsFor();

//================================#14.2=========================================

//! H/w 1. Loops, task #14.2. WHILE
//! Calculate the partial product: 1 + (sin(kx) / k!)
long double ProductNthWhile(float x, int n);

//! H/w 1. Loops, task #14.2. DO-WHILE
//! Calculate the partial product: 1 + (sin(kx) / k!)
long double ProductNthDoWhile(float x, int n);

//! H/w 1. Loops, task #14.2. FOR
//! Calculate the partial product: 1 + (sin(kx) / k!)
long double ProductNthFor(float x, int n);

//================================#14.3=========================================

//! H/w 1. Loops, task #14.3. WHILE
//! Calculate tg(x)/x^2 in range [-5;5] by step h = 0.1.
void TanXinRangeWhile();

//! H/w 1. Loops, task #14.3. DO-WHILE
//! Calculate tg(x)/x^2 in range [-5;5] by step h = 0.1.
void TanXinRangeDoWhile();

//! H/w 1. Loops, task #14.3. FOR
//! Calculate tg(x)/x^2 in range [-5;5] by step h = 0.1.
void TanXinRangeFor();

//================================#15.1=========================================

//! H/w 1. Loops, task #15.1. WHILE
//! Calculate the sum of the square roots of even numbers in range [1;50]
double SumSqrtsWhile();

//! H/w 1. Loops, task #15.2. DO-WHILE
//! Calculate the sum of the square roots of even numbers in range [1;50]
double SumSqrtsDoWhile();

//! H/w 1. Loops, task #15.3. FOR
//! Calculate the sum of the square roots of even numbers in range [1;50]
double SumSqrtsFor();

//================================#15.2=========================================

//! H/w 1. Loops, task #15.2. WHILE
//! Calculate a n-th partial sum of series: (1/i! + sqrt(|x|)
double PartialSumNthWhile(int n, float x);

//! H/w 1. Loops, task #15.2. DO-WHILE
//! Calculate a n-th partial sum of series: (1/i! + sqrt(|x|)
double PartialSumNthDoWhile(int n, float x);

//! H/w 1. Loops, task #15.2. FOR
//! Calculate a n-th partial sum of series: (1/i! + sqrt(|x|)
double PartialSumNthFor(int n, float x);

//================================#15.3=========================================

//! H/w 1. Loops, task #15.3. WHILE
//! Calculates tg(x)/(x + 2)^2 in range [-5;5] by step h = 0.1.
void TangentXinRangeWhile();

//! H/w 1. Loops, task #15.3. DO-WHILE
//! Calculates tg(x)/(x + 2)^2 in range [-5;5] by step h = 0.1.
void TangentXinRangeDoWhile();

//! H/w 1. Loops, task #15.3. FOR
//! Calculates tg(x)/(x + 2)^2 in range [-5;5] by step h = 0.1.
void TangentXinRangeFor();

//================================#17.1=========================================

//! H/w 1. Loops, task #17.1. WHILE
//! Calculate the sum of the negative odd numbers in range [-20;20]
int SumNegativesWhile();

//! H/w 1. Loops, task #17.1. DO-WHILE
//! Calculate the sum of the negative odd numbers in range [-20;20]
int SumNegativesDoWhile();

//! H/w 1. Loops, task #17.1. FOR
//! Calculate the sum of the negative odd numbers in range [-20;20]
int SumNegativesFor();

//================================#17.2=========================================

//! H/w 1. Loops, task #17.2. WHILE
//! Calculate a n-th partial sum of series: (-1)^k*(k+1) / k!
double PartNthSumWhile(int n);

//! H/w 1. Loops, task #17.2. DO-WHILE
//! Calculate a n-th partial sum of series: (-1)^k*(k+1) / k!
double PartNthSumDoWhile(int n);

//! H/w 1. Loops, task #17.2. FOR
//! Calculate a n-th partial sum of series: (-1)^k*(k+1) / k!
double PartNthSumFor(int n);

//================================#17.3=========================================

//! H/w 1. Loops, task #17.3. WHILE
//! Calculates 4e^x / x^2 in range [-7;7] by step h = 0.3.
void ExpXinRangeWhile();

//! H/w 1. Loops, task #17.3. DO-WHILE
//! Calculates 4e^x / x^2 in range [-7;7] by step h = 0.3.
void ExpXinRangeDoWhile();

//! H/w 1. Loops, task #17.3. FOR
//! Calculates 4e^x / x^2 in range [-7;7] by step h = 0.3.
void ExpXinRangeFor();
