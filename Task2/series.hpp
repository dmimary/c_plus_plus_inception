﻿#pragma once

//! Calcultes partial harmonic series (1/k) from k=1 to for N
float harmonic(int N);

//! Calculates the factorial to use in the "product" function
int factorial(int N);

//! Calculates partial product (2 + 1/i!) from i = 1 to 10
float product();

//! Tabulation of x^2 function in range [a, b] by 0.1 step
void tabulation(float a, float b);
