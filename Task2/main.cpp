﻿/*!
    Examples of calculating partial series (sum and product).

    @author Maria Dmitrieva
*/

#include <iostream>
#include "series.hpp"

using std::cin;
using std::cout;
using std::endl;

int main()
{
    // harmonic func call
    int N;
    cout << "1. Harmonic series. Input N: ";
    cin >> N;
    cout << "Result = " << harmonic(N) << endl << endl;

    // product func call
    cout << "2. Partial product (2 + 1/i!) from i = 1 to 10 equals "
         << product() << endl << endl;

    // tabulation func call
    float a, b;
    cout << "3. Tabulation f(x) = x^2 in range [a, b] by step 0.1.\n";
    cout << "Input a, b: ";
    cin >> a >> b;
    cout << "Result: " << endl;
    tabulation(a, b);
    cout << endl;

    return 0;
}
