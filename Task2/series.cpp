﻿#include <iostream>
#include "series.hpp"

// Calcultes partial harmonic series (1/k) from k=1 to for N
float harmonic(int N) {
    float sum = 0.0;
    for (int k = 1; k <= N; k++){
        sum += 1.0 / k;
    }

    return sum;
}

// Calculates the factorial to use in the "product" function
int factorial(int N) {
    int fact = 1;
    for (int i = 2; i <= N; i++){
        fact *= i;
    }

    return fact;
}

// Calculates partial product (2 + 1/i!) from i = 1 to 10
float product() {
    float product = 1.0;
    for (int i = 1; i <= 10; i++){
        product *= 2 + 1 / factorial(i);
    }

    return product;
}

// Tabulation of x^2 function in range [a, b] by 0.1 step
void tabulation(float a, float b) {
    float result;
    float x = a;
    while (x <= b){
        result = x * x;
        std::cout << "x = " << x << " -> f(x) = " << result << std::endl;
        x += 0.1;
    }
}
