﻿#include "2d_list.hpp"

#include <iostream>

using std::cout;
using std::endl;

int main()
{
    TNode* _begin = NULL;
    TNode* _end = NULL;

    init(&_begin, &_end);

    cout << "2D List[" << LIST_QTY << "] was initialized by: " << endl;
    display(_begin);

    int value = 70;

    if ( insert_after_current(&_begin, &_end, value) )
            display(_begin);
    else error_find(value);

    value = -30;

    if ( del(&_begin, &_end, value) )
        display(_begin);
    else error_find(value);

    return 0;
}
