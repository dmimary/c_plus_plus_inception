﻿#pragma once
#include <cstddef> // std::size_t

using std::size_t;

const size_t LIST_QTY = 10;

struct TNode {
    int data;
    TNode* p_prev;
    TNode* p_next;
};

//! Create a 2D-list.
static void create(TNode** begin, TNode** end, int _data);

//! Add new item to the 2D-list.
void add(TNode** begin, TNode** end, int _data);

//! Find element in a list by key - data.
TNode* find(TNode* p_current, const int& key);

void error_find(const int& key);

//! Find element in a list by index.
TNode* find_by_index(TNode* p_current, const size_t& key);

//! Remove item from the 2D-list by key using find() func.
bool del(TNode** begin, TNode** end, int key);

//! Add new element after current.
bool insert_after_current(TNode** begin, TNode** end, const int& key);

//! Initialize list.
void init(TNode** begin, TNode** end);

//! Display list t user in terminal.
void display(TNode* current);
