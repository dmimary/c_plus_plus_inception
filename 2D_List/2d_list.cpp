﻿#include "2d_list.hpp"

#include <iostream>
#include <cstdlib>  // rand()

using std::cout;
using std::cin;
using std::endl;
using std::rand;

static void create(TNode** begin, TNode** end, int _data) {
    *begin = new TNode;
    (*begin)->data = _data;
    (*begin)->p_prev = NULL;
    (*begin)->p_next = NULL;
    *end = *begin;
}

void add(TNode** begin, TNode** end, int _data) {
    if (*end == NULL) {
        create(end, begin, _data);
    } else {
        TNode* p_value = new TNode;
        p_value->data = _data;
        p_value->p_next = NULL;
        p_value->p_prev = *end;
        (*end)->p_next = p_value;
        *end = p_value;
    }
}

TNode* find(TNode* p_current, const int& key) {
    while ((p_current != NULL) && (p_current->data != key)) {
        p_current = p_current->p_next;
    }

    return p_current;
}

TNode* find_by_index(TNode* p_current, const size_t& key);

bool del(TNode** begin, TNode** end, int key) {

    TNode* p_current = find(*begin, key);

    if (p_current == NULL)
        return false;

    int value = p_current->data;

    if (p_current == *begin) {
        *begin = (*begin)->p_next;
        (*begin)->p_prev = NULL;
    } else
    if (p_current == *end) {
        *end = (*end)->p_prev;
        (*end)->p_next = NULL;
    } else {
        (p_current->p_prev)->p_next = p_current->p_next;
        (p_current->p_next)->p_prev = p_current->p_prev;
    }

    cout << "Element " << value << " was removed from the list." << endl
         << endl;

    delete p_current;
    return true;
}

bool insert_after_current(TNode** begin, TNode** end, const int& key) {

    TNode* p_current = find(*begin, key);

    if (p_current == NULL) {
        return false;
    } else {
        if (p_current == *end) {
            add(begin, end, key);  // int data != const int& in init() func int key
        } else {
            TNode* p_value = new TNode;
            p_value->data = (rand() % 198) - 99;
            p_value->p_next = p_current->p_next;
            p_value->p_prev = (p_current->p_next)->p_prev;
            (p_current->p_next)->p_prev = p_value;
            p_current->p_next = p_value;
        }

    }
    cout << "New element was inserted after the " << key << " one."
         << endl << endl;
    return true;
}

void init(TNode** begin, TNode** end) {
    for (int i = 0; i < LIST_QTY; i++) {
        add(begin, end, (rand() % 198) - 99);
    }
}

void display(TNode* current) {
    cout << "List: [ ";
    while (current != NULL) {
        cout << current->data << " ";
        current = current->p_next;
    }
    cout << "]" << endl << endl;
}

void error_find(const int& key) {
    cout << "Element " << key << " wasn't found." << endl;
}
