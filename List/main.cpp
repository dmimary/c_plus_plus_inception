﻿#include "list.hpp"

#include <iostream>

using std::cout;
using std::endl;

int main()
{
    TNode** _begin = NULL;
    TNode** _end = NULL;
    _begin = new TNode*;
    _end = new TNode*;

    init_list(_end);

    display_list(*_begin);

    return 0;
}
