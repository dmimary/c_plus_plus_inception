﻿#pragma once
#include <cstddef> // std::size_t

using std::size_t;

const unsigned int LIST_QTY = 10;

struct TNode {
    int data;  // data field
    TNode* p_next;  // pointer on node elem
};

//! Create a list
static void create(TNode** begin, TNode** end, int d);

//! Add new element in the end of list
void add(TNode** p_end, int _data);

//! Delete the first elem in a list
bool del(TNode** p_begin, TNode** p_end, int key, int& value);

//! Find the element of list by key
TNode* find(TNode** p_prev, TNode* p_begin, int key);

//! Add elem after the current
bool add_after_current(TNode** p_begin, TNode** p_end, int _data);

//! Initialize a list by random values
void init_list(TNode** p_end);

//! Display the list.
void display_list(TNode* p_current);
