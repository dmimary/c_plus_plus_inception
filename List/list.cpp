﻿#include "list.hpp"

#include <iostream>
#include <cstdlib>  // rand()

using std::cout;
using std::cin;
using std::endl;
using std::rand;

static void create(TNode** begin, TNode **end, int d) {
    *begin = new TNode;
    (*begin)->data = d;
    (*begin)->p_next = NULL;
    *end = *begin;
}

void add(TNode** p_end, int _data) {
    if (*p_end == NULL) {
        create(p_end, p_end, _data);
    } else {
        TNode* p_value = new TNode;
        p_value->data = _data;
        p_value->p_next = NULL;
        (*p_end)->p_next = p_value;
        *p_end = p_value;  // ???
    }
}

bool del(TNode** p_begin, TNode** p_end, int key, int& value) {
    TNode *p_prev = NULL;
    TNode *p_current = find(&p_prev, *p_begin, key);

    if (p_current == NULL) {
        return false;
    }

    value = p_current->data;

    if(p_current == *p_begin) {
        *p_begin = (*p_begin)->p_next;
    } else if (p_current == *p_end) {
        *p_end = p_prev;
        (*p_end)->p_next = NULL;
    } else {
        p_prev->p_next = p_current->p_next;
    }

    delete p_current;
    return true;
}

TNode* find(TNode** p_prev, TNode* p_current, int key){

    while ((p_current->data != key) && (p_current != NULL)) {
        *p_prev = p_current;
        p_current = p_current->p_next;
}

    return p_current;
}

bool add_after_current(TNode** p_begin, TNode** p_end, int _data) {
    TNode *p_prev = NULL;
    TNode* p_current = find(&p_prev, *p_begin, _data);
    if (p_current == NULL) {
        return false;
    } else {
        TNode* p_value = new TNode;
        p_value->data = _data;
        p_value->p_next = p_current->p_next;

        if (p_current == *p_end) {
            *p_end = p_value;
        }

        p_current->p_next = p_value;

    }

    return true;
}

void init_list(TNode** p_end) {

    for (int i = 0; i < LIST_QTY; i++) {
        add(p_end, (rand() % 198) - 99);
    }
}

void display_list(TNode* p_current) {
    while (p_current != NULL) {
        cout << "List: " << p_current->data << " ";
        p_current = p_current->p_next;
    }

    cout << endl;
}
