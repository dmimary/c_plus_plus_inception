﻿#pragma once

const unsigned int QUEUE_QTY = 10;

struct TNode {
    int data;  // data field
    TNode* ptr;  // pointer on noded elem
};

//! Create a queue
void create(TNode** begin, TNode** end, int d);

//! Add the new elem in a queue
void add(TNode** end, int d);

//! Delete the first elem in queue
int del(TNode** begin);

//! Initialize the queue
void init_queue(TNode** end);

//! Find elem in a queue by value
TNode* search(TNode** begin, int value);

//! Delete elem which was found by search() func
void delete_elem();
