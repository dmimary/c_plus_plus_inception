﻿#include <iostream>
#include "queue.hpp"

using std::cout;
using std::endl;

int main()
{
    TNode** _begin = NULL;
    TNode** _end = NULL;
    _begin = new TNode*;
    _end = new TNode*;
    int _data = 55;

    create(_begin, _end, _data);

    _data = 11;

    add(_end, _data);

    cout << "Queue contains: " << (*_begin)->data << endl;

    cout << "Top elem " << del(_begin) << " was popped." << endl;

    cout << "Queue contains: " << (*_end)->data << endl;

    cout << "Top elem " << del(_begin) << " was popped." << endl;

    return 0;
}
