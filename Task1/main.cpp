﻿/*!
    Calculates the roots of quadratic equation ax^2 + bx + c = 0.
    Input: a, b, c.
    Output: x1, x2.

    @author Mary Dmitrieva
*/

#include <iostream>
#include <cstdio>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    float a, b, c, D, x1, x2;
    cout << "Input a b c: ";
    cin >> a >> b >> c;
    cout << a << "x^2 + " << b << "x + " << c << " = 0" << endl;
    D = pow(b, 2) - 4 * a * c;  // Calculation of the discriminant

    if (D < 0) {
        cout << "There are no real roots" << endl;

        return 0;
    }
    else if (D == 0) {
        x1 = -b / (2 * a);
        x2 = x1;
    }
    else {
        x1 = (-b + sqrt(D)) / (2 * a);
        x2 = (-b - sqrt(D)) / (2 * a);
    }

    cout << "x1 = " << x1 << endl << "x2 = " << x2 << endl;
    return 0;
}
