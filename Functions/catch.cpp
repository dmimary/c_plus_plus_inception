﻿#include <iostream>
#include <cassert>  // include assert

#include "catch.hpp"
#include "functions.hpp"  // all functions realisation

using namespace Catch;

//=================================TASK#4=======================================

// Testing is_ascending func.
void check_is_ascending(const int arr1[N], const int arr2[N]) {
    assert(is_ascending(arr1) == true);
    assert(is_ascending(arr2) == false);
}

// Testing set_to_zero func.
void check_set_to_zero(int arr[Catch::N]) {
    set_to_zero(arr);
    for (int i = 0; i < N; i++) {
        assert(arr[i] == 0);
    }
}

//=================================TASK#9=======================================

// Testing the max_matrix_elem func.
void check_max_matrix_elem(const int arr_A[N][M], const int arr_B[N][M]) {
// Variables to store indices max rows and columns of two matrices A & B.
    unsigned short int max_row_A = 0;
    unsigned short int max_col_A = 0;
    unsigned short int max_row_B = 0;
    unsigned short int max_col_B = 0;

    assert(max_matrix_elem(arr_A, max_row_A, max_col_A) == 90);
    assert(max_row_A == 4 && max_col_A == 2);
    assert(max_matrix_elem(arr_B, max_row_B, max_col_B) == 99);
    assert(max_row_B == 3 && max_col_B == 0);
}

// Testing the swap_max_matrices_elems func.
void check_swap_max_matrices_elems(int arr_A[N][M], int arr_B[N][M]) {
    assert(arr_A[4][2] == 99);
    assert(arr_B[3][0] == 90);
}

//=================================TASK#10======================================
// Note! 20! is max for unsigned long long data type.
// Testing the factorial func.
void check_factorial() {
    assert(factorial(0) == 1);
    assert(factorial(5) == 120);
    assert(factorial(10) == 3628800);
    assert(factorial(20) == 2432902008176640000);
}

// Testing the combinators func.
void check_combinations() {
    assert(combinations(20, 5) == 15504);
    assert(combinations(10, 10) == 1);
    assert(combinations(2, 12) == 0);
}

//=================================TASK#14======================================

// Note! a = 33 is max for unsigned long long data type.
// Testing the product func.
void check_product() {
    assert(product(4) == 8);
    assert(product(5) == 15);
    assert(product(10) == 3840);
    assert(product(13) == 135135);
    assert(product(33) == 6332659870762850625);
}

// Testing the formula func.
void check_formula() {
    assert(formula(5, 1) == 0.5);
    assert(formula(3, 3) == -0.5);
    assert(formula(6, 8) == 0);
}

//=================================TASK#17======================================

// Testing the bin func
void check_bin() {
    assert(bin(1) == 1);
    assert(bin(2) == 10);
    assert(bin(3) == 11);
    assert(bin(7) == 111);
    assert(bin(10) == 1010);
    assert(bin(222) == 11011110);
    assert(bin(256) == 100000000);
    assert(bin(341) == 101010101);
    assert(bin(1024) == 10000000000);
}
