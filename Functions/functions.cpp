﻿#include <iomanip>   // include setw()
#include <iostream>  // include std::cout, std::endl

#include "functions.hpp"

using std::cout;
using std::endl;
using std::setw;
using namespace Funcs;

// Is array sorrted in ascending order or not?
bool is_ascending(const int arr[N]) {
    for (size_t i = 0; i < N - 1; i++)
        if (arr[i] > arr[i + 1])
            return false;  // To show that array isn't sorted

    return true;  // To show that array is sorted
}

// Set all elements of an array to zero,
// if the array is not sorted in ascending order
void set_to_zero(int arr[N]) {
    if (!is_ascending(arr))
        for (size_t i = 0; i < N; i++) arr[i] = 0;
}

// Find a max elem of matrix.
// Return value of max elem, row and column indices.
int max_matrix_elem(const int arr[N][M], unsigned short int& row,
                           unsigned short int& column) {
    int result = arr[0][0];
    for (size_t i = 0; i < N; i++)
        for (size_t j = 0; j < M; j++)
            if (arr[i][j] > result) {
                row = i;
                column = j;
                result = arr[i][j];
            }

    return result;
}

// Swap max elems of A & B matrices
void swap_max_matrices_elems(int A[N][M], int B[N][M]) {
    // Variables to store indices max rows and columns of two matrices A & B
    unsigned short int max_row_A = 0;
    unsigned short int max_col_A = 0;
    unsigned short int max_row_B = 0;
    unsigned short int max_col_B = 0;

    // Find max elems of matrices
    int max_A = max_matrix_elem(A, max_row_A, max_col_A);
    int max_B = max_matrix_elem(B, max_row_B, max_col_B);
    int temp = max_A;

    A[max_row_A][max_col_A] = max_B;
    B[max_row_B][max_col_B] = temp;
}

// NOTE! Unsigned long long can stor max 20! otherwise there will overflow.
// Calculate factorial. Recursive function.
unsigned long long factorial(unsigned int n) {
    unsigned long long result = 1;

    for (size_t i = 2; i <= n; i++) {
        result *= i;
    }

    return result;
}

// Calculate a number of k-combinations by formula: n! / k!(n-k)!
// k objects are selected from a set of n objects (n >= k, if k > n then = 0)
unsigned long long combinations(unsigned int n, unsigned int k) {
    if (k > n)
        return 0;
    else {
        return factorial(n) / (factorial(k) * factorial(n - k));
    }
}

// Calculate x!!, х!! = 1 * 3 * 5 *...* х, if x is odd
//                х!! = 2 * 4 * 6 *...*х, if x is even
// Note! a = 33 is max for unsigned long long data type
unsigned long long product(unsigned int x) {
    unsigned long long product = 1;

    if (x % 2 == 0)  // If x is even
        for (size_t i = 2; i <= x; i += 2) product *= i;
    else  // If x is odd
        for (size_t i = 3; i <= x; i += 2) product *= i;

    return product;
}

// Calculate (a!! - ab) / (a!! + ab), where a & b are natural numbers
double formula(unsigned int a, unsigned int b) {
    double prod = product(a);
    return (prod - a * b) / (prod + a * b);
}

// Convert the decimal number to binary
unsigned long long bin(unsigned int x) {

    unsigned long long result = 1;
    unsigned long long remainder = 0;
    unsigned long long temp = 1;

    for (int i = x; i > 1; i /= 2) {
        remainder = result % temp;

        if (i % 2 == 0)
            result = 10 * temp + remainder;
        else
            result = 11 * temp + remainder;

        temp *= 10;
    }

    return result;
}
