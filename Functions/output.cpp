﻿#include <iomanip>   // include std::setw
#include <iostream>  // include std::cout, std::cin, std::endl
#include <string>    // include std::string

#include "output.hpp"
#include "catch.hpp"
#include "functions.hpp"

using std::cin;
using std::cout;
using std::endl;
using std::setw;
using std::string;

static const string separator = string(80, '~');

// Display an array.
void display_array(const int arr[N]) {
    for (size_t i = 0; i < N; i++) cout << arr[i] << " ";
}

// Display a matrix.
void display_matrix(const int arr[N][M]) {
    for (size_t i = 0; i < N; i++) {
        cout << endl << std::setw(2) << i << '|';
        for (size_t j = 0; j < M; j++) cout << std::setw(3) << arr[i][j] << ' ';
    }
}

//=================================TASK#4=======================================

void show_set_to_zero() {
    int array_1[N] = {-8, -3, 0, 1, 5};
    int array_2[N] = {-31, -20, -23, -14, 0};

    // Testing is_ascending func
    check_is_ascending(array_1, array_2);

    // Display examples of a set_to_zero func
    cout << separator << endl
         << "Task 4. Set all elements of an array to zero, if it's not sorted "
            "in ascending\norder. Checking two arrays:\n"
         << separator << endl << endl;

    cout << "   array A: [ ";
    display_array(array_1);
    cout << "] -> [ ";
    set_to_zero(array_1);
    display_array(array_1);
    cout << "]\n" << endl;

    cout << "   array B: [ ";
    display_array(array_2);
    cout << "] -> [ ";
    set_to_zero(array_2);
    display_array(array_2);
    cout << "]\n" << endl;

    check_set_to_zero(array_2);
}

//=================================TASK#9=======================================

void show_swap_max_matrices_elems() {

    // N - rows, M - columns
    int matrix_A[N][M] = {{-8, -3, 0, 1},
                          {9, 11, -8, 0},
                          {0, -7, 19, -1},
                          {22, 4, -2, 13},
                          {3, 0, 90, -22}};

    // M - rows, N - columns
    int matrix_B[N][M] = {{-8, -3, 0, 1},
                          {9, 11, -8, 0},
                          {0, -7, 19, -1},
                          {99, 4, -2, 13},
                          {7, 31, 22, 11}};

    // Display the given matrices.
    const string format = "\n   " + string(15, '_');

    cout << separator << endl
         << "Task 9. Swap max elems of 2 matrices. Initial matrices:\n"
         << separator << endl << endl
         << setw(15) << "Matrix A:" << format;
    display_matrix(matrix_A);
    cout << endl << endl << "The max elem A[4][2] = 90" << endl << endl
         << setw(15) << "Matrix B:" << format;
    display_matrix(matrix_B);
    cout << endl << endl << "The max elem B[3][0] = 99" << endl << endl;

    // Testing the max_matrix_elem func.
    check_max_matrix_elem(matrix_A, matrix_B);

    // Swap max matrices elems
    swap_max_matrices_elems(matrix_A, matrix_B);

    // Testing the swap_max_matrices_elems func.
    check_swap_max_matrices_elems(matrix_A, matrix_B);

    // Display the changed matrices
    cout << "Swap max elems 90 <-> 99. Changed matrices:\n" << endl
         << setw(15) << "Matrix A:" << format;
    display_matrix(matrix_A);
    cout << endl << endl << setw(15) << "Matrix B:" << format;
    display_matrix(matrix_B);
    cout << endl << endl;
}

//=================================TASK#10======================================

void show_combinations() {
    check_factorial();

    check_combinations();

    cout << separator << endl
         << "Task 10.  Calculat the number of k-combinations by formula:\n"
            "C(n,k) = n! / k!(n-k)!\nk objects are selected from a set of n "
            "objects (n >= k, if k > n then = 0)" << endl
         << separator << endl << endl;

    unsigned long long comb = combinations(2, 2);

    // Display the examples of combinations func
    cout << "1. Number of 2-combinations from set of 2 numbers: C(2,2) = "
         << comb << endl;

    comb = combinations(5, 1);
    cout << "2. Number of 1-combinations from set of 5 numbers: C(5,1) = "
         << comb << endl;

    comb = combinations(20, 10);
    cout << "3. Number of 20-combinations from set of 10 numbers: C(20,10) = "
         << comb << endl << endl;
}

//=================================TASK#14======================================
void show_formula() {
    // Testing product func
    check_product();

    check_formula();

    cout << separator << endl
         << "Task 14. Calculate (a!! - ab) / (a!! + ab), where" << endl
         << "х!! = 1 * 3 * 5 *...* х, if x is odd;" << endl
         << "х!! = 2 * 4 * 6 *...*х, if x is even." << endl;
    cout << separator << endl << endl;

    cout << "1. (5!! - 5*1) / (5!! + 5*1) = " << formula(5, 1) << endl;
    cout << "2. (3!! - 3*3) / (3!! + 3*3) = " << formula(3, 3) << endl;
    cout << "3. (11!! - 11*945) / (11!! + 11*945) = " << formula(11, 945)
         << endl <<endl;
}

//=================================TASK#17======================================

void show_bin() {
    const string underline = string(65, '-');

    check_bin();

    cout << separator << endl
         << "Task 17. Convert given decimal numbers a b c to binary." << endl
         << separator << endl
         << setw(9) << "|" << setw(9) << "a" << setw(10) << "|" << setw(10)
         << "b" << setw(10) << "|" << setw(10) << "c"<< endl
         << underline << endl << "decimal |"
         << setw(9) << "5" << setw(10) << "|" << setw(10) << "7" << setw(10)
         << "|" << setw(10) << "1024" << setw(10) << endl << underline << endl
         << "binary  |  " << setw(7) << bin(5) << setw(10) << "|" << setw(10)
         << bin(7) << setw(10) << "|" << setw(13) << bin(1024) << endl << endl;
}

//=================================THE END======================================

void the_end() {
    cout << separator << endl << setw(40) << "THE END" << endl
         << separator << endl;
}
