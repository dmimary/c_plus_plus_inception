﻿#pragma once

#include <cstddef>  // include size_t

const std::size_t N = 5;
const std::size_t M = 4;

//! Display an array.
void display_array(const int arr[N]);

//! Display a matrix.
void display_matrix(const int arr[N][M]);

//=================================TASK#4=======================================

void show_set_to_zero();

//=================================TASK#9=======================================

void show_swap_max_matrices_elems();

//=================================TASK#10======================================

void show_combinations();

//=================================TASK#14======================================

void show_formula();

//=================================TASK#17======================================

void show_bin();

//=================================THE END======================================

void the_end();
