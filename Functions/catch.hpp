﻿#pragma once

#include <cstddef>  // include size_t

namespace Catch {

    const std::size_t N = 5;
    const std::size_t M = 4;
}

//=================================TASK#4=======================================

void check_is_ascending(const int arr1[Catch::N], const int arr2[Catch::N]);

void check_set_to_zero(int arr[Catch::N]);

//=================================TASK#9=======================================

void check_max_matrix_elem(const int arr_A[Catch::N][Catch::M],
                           const int arr_B[Catch::N][Catch::M]);

void check_swap_max_matrices_elems(int arr_A[Catch::N][Catch::M],
                                   int arr_B[Catch::N][Catch::M]);

//=================================TASK#10======================================

void check_factorial();

void check_combinations();

//=================================TASK#14======================================

void check_product();

void check_formula();

//=================================TASK#17======================================

void check_bin();
