﻿#pragma once

#include <cstddef>  // include size_t

namespace Funcs {

    const std::size_t N = 5;
    const std::size_t M = 4;
}

//! Define wether array is sorrted in ascending order or not.
bool is_ascending(const int arr[Funcs::N]);

//! Set all elements of an array to zero,
//! if the array is not sorted in ascending order
void set_to_zero(int arr[Funcs::N]);

//! Find a max elem of matrix.
//! Return value of max elem, row and column indices.
int max_matrix_elem(const int arr[Funcs::N][Funcs::M], unsigned short int &row,
                    unsigned short int &column);

//! Swap max elems of A & B matrices
void swap_max_matrices_elems(int A[Funcs::N][Funcs::M],
                             int B[Funcs::N][Funcs::M]);

//! Calculate factorial of n <= 20.
unsigned long long factorial(unsigned int n);

//! Calculate a number of k-combinations by formula: n! / k!(n-k)!
//! k objects are selected from a set of n objects (n >= k, if k > n then = 0)
unsigned long long combinations(unsigned int n, unsigned int k);

//! Calculate x!!, х!! = 1 * 3 * 5 *...* х, if x is odd
//!                х!! = 2 * 4 * 6 *...*х, if x is even
unsigned long long product(unsigned int x);

//! Calculate (a!! - ab) / (a!! + ab), where a b are natural numbers
double formula(unsigned int a, unsigned int b);

//! Convert the decimal number to binary
unsigned long long bin(unsigned int x);
