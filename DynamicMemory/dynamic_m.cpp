﻿#include "dynamic_m.hpp"

#include <iostream>  // std::cout, std::endl
#include <string>  // std::string
#include <cstdlib>  // rand()
#include <iomanip>  // std::setw()

using std::cout;
using std::endl;

void display_refs(int* a, int* b) {
    const std::string separator  = std::string(27, '~');

    cout << "address a = " << a << endl
         << "address b = " << b << endl
         << "value a = " << *a << endl
         << "value b = " << *b << endl
         << separator << endl;
}

int sum(int a, int b) {
    return a + b;
}

int sum_sqr(func_pointer p_sum_sqr, int a, int b) {
    return p_sum_sqr(a, b) * p_sum_sqr(a, b);
}

void order(int& x, int& y) {
    if (x > y) {
        int temp = x;
        x = y;
        y = temp;
    }
}

void sort_array(func_ptr_ref p_order, int* arr) {
    for (unsigned int i = 0; i < COLUMNS_QTY - 1; i++)
        for (unsigned int j = i + 1; j < COLUMNS_QTY; j++) {
            p_order(arr[i], arr[j]);
        }
}

void init_array(int* arr) {
    for (unsigned int i = 0; i < COLUMNS_QTY; i++) {
        arr[i] = (rand() % 198) - 99;
    }
}

void display_array(const int *arr) {
    for (unsigned int i = 0; i < COLUMNS_QTY; i++) {
        cout << "array[" << i << "] = " << arr[i] << endl;
    }
}

void init_matrix(int** arr_2D) {
for (unsigned int i = 0; i < ROWS_QTY; i++)
    for (unsigned int j = 0; j < COLUMNS_QTY; j++) {
        arr_2D[i][j] = (rand() % 198) - 99;
    }
}

void display_matrix(int** arr_2D) {
        for (unsigned int i = 0; i < ROWS_QTY; i++) {
            cout << endl << std::setw(2) << i << '|';
            for (unsigned int j = 0; j < COLUMNS_QTY; j++)
                cout << std::setw(3) << arr_2D[i][j] << ' ';
        }
}

int min_elem(int** arr_2D, unsigned short int& row, unsigned short int& col) {
    int min = arr_2D[0][0];

    for (unsigned int i = 0; i < ROWS_QTY; i++)
        for (unsigned int j = 0; j < COLUMNS_QTY; j++) {
            if (arr_2D[i][j] < min) {
                row = i;
                col = j;
                min = arr_2D[i][j];
            }
        }

    return min;
}
