﻿#include "dynamic_m.hpp"

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
//====================== Pointers munipulations ================================
    cout << "=== Pointers munipulations ===" << endl << endl;
    // Pointers to an integer
    int* a = NULL;
    int* b = NULL;

    // Requests for dynamic memory allocation
    a = new int;
    b = new int;
    // To assign a value to pointer use dereference operator (*)
    *a = 19;
    *b = -19;

    display_refs(a, b);
    cout << endl;

    *a = *b;

    display_refs(a, b);
    cout << endl;

    delete a;
    delete b;

    display_refs(a, b);
    cout << endl;

    int c = 13;

    b = &c;  //  pointer b = address c using address-of operator(&)

    display_refs(a, b);
    cout << "address c = " << b << endl
         << "value c = " << *a << endl;

//=========================== Function Pointers ================================

    cout << endl << "=== Function Pointers ===" << endl << endl;
    int (*p_sum) (int, int) = NULL;
    p_sum = &sum;

    int result = p_sum(3, 7);

    cout << "p_sum(3, 7) = " << result << endl;

    int sqr = sum_sqr(p_sum, 3, 7);

    cout << "sum_sqr(p_sum, 3, 7) = " << sqr << endl << endl;


//============================== Array Pointers ================================
    cout << endl << "=== Array Pointers ===" << endl << endl
         << "Array[N], N = " << COLUMNS_QTY << ":" << endl;

    int *arr = new int[COLUMNS_QTY];
    init_array(arr);

    result = 0;

    for (int i = 0; i < COLUMNS_QTY; i++) {
        result += arr[i];
    }

    display_array(arr);

    cout << endl << "Sum of array elements = " << result << endl << endl;

    void (*p_order) (int&, int&) = NULL;
    p_order = &order;

    sort_array(p_order, arr);

    cout << "Sorted array in ascending order: " << endl << endl;

    display_array(arr);

    delete[] arr;

//============================== Matrix Pointers ===============================
    cout << endl << "=== Matrix / 2D array Pointers ===" << endl << endl
         << "Matrix["  << ROWS_QTY << "][" << COLUMNS_QTY << "]:" << endl;

    int **matrix = new int*[ROWS_QTY];

    for (int i = 0; i < ROWS_QTY; i++) {
        matrix[i] = new int[COLUMNS_QTY];
    }

    init_matrix(matrix);

    display_matrix(matrix);

    unsigned short int row = 0;
    unsigned short int col = 0;

    int min = min_elem(matrix, row, col);

    cout << endl << endl
         << "Min matrix elem is matrx[" << row << "][" << col << "] = "
         << min << endl << endl;

    delete[] matrix;

    return 0;
}
