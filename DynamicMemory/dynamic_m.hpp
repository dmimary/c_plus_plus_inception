﻿#pragma once
// alias for complex type name
typedef int(*func_pointer) (int, int);
typedef void(*func_ptr_ref) (int&, int&);

const unsigned short int ROWS_QTY = 3;
const unsigned short int COLUMNS_QTY = 10;

//! Display address and value of var-pointers a and b
void display_refs(int* a, int* b);

//! Calculate SUM(a, b). Easy-peasy.
int sum(int a, int b);

//! Function pointer.
//! Calculate square root of sum() func result.
int sum_sqr(func_pointer p_sum_sqr, int a, int b);

//! Sort two numbers x and y in ascending order. Piece of cake.
void order(int& x, int& y);

//! Sort array in ascending order using pointer to order() func
void sort_array(func_ptr_ref p_order, int* arr);

//! Initialize an array by random integers [99,-99].
void init_array(int* arr);

//! Display an array.
void display_array(const int* arr);

//! Initialize a matrix by random integers [99,-99].
void init_matrix(int** arr_2D);

//! Display a matrix.
void display_matrix(int** arr_2D);

//! Find min elem in 2D array and save number of its row and column.
int min_elem(int** arr_2D, unsigned short &row, unsigned short &col);
