﻿#include "fstream.hpp"

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    const char f_name[] = "input.txt";
    const char f_name_[] = "output.txt";
    char buf[BUFFER_SIZE] = "";

    read_line_by_line(f_name, sizeof(f_name), buf);

    write_to_file(f_name_, sizeof(f_name_));

    return 0;
}
