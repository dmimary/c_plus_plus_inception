﻿#include "fstream.hpp"

#include <iostream>
#include <fstream>
#include <string>

using std::cin;
using std::cout;
using std::endl;

void read_line_by_line(const char* f_name, size_t length, char* buffer) {

    std::ifstream file(f_name);
    if (!file) {
        std::cerr << "Error oppening a file!" << endl;
        std::exit(EXIT_FAILURE);
    }

    do {
       file.getline(buffer, BUFFER_SIZE);
       cout << buffer << endl;
   } while (!file.eof());
}

void write_to_file(const char* f_name, size_t length) {

    std::ofstream outfile;
    std::string user_text = "";

    // If there is no file with this name, the new file will be created
    outfile.open("output.txt", std::ofstream::app);

    cout << "Please, input some text to write it on a output.txt file: " << endl;
    std::getline(cin, user_text);

    outfile << user_text << endl;

    outfile.close();
}
