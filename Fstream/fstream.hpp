﻿#pragma once

#include <iomanip>

using std::size_t;

const size_t BUFFER_SIZE = 81;

//! Read from input.txt file and display line by line.
void read_line_by_line(const char* f_name, size_t length, char* buffer);

//! Write on output.txt file strings given by user.
void write_to_file(const char* f_name, size_t length);
