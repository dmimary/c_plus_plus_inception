﻿#pragma once

#include "complex.hpp"
// TODO: Remove this struct and move all features in list struct to follow SRP
struct TElement {

private:
    TComplex data;
    TElement* p_prev;
    TElement* p_next;

public:
    TElement();
    TElement(TComplex _data);
    TElement(TComplex _data, TElement* _p_prev, TElement* _p_next);

    void set_data(TComplex val);
    void set_p_prev(TElement* val);
    void set_p_next(TElement* val);

    TComplex get_data();
    TElement* get_p_prev();
    TElement* get_p_next();

    void save_to_file(std::string file_name);
    void load_from_file(std::string file_name, int pos_in_file);
    void clear_file(std::string file_name);

    friend std::ostream& operator << (std::ostream& os, const TComplex& _cn);
};
