﻿#include "element.hpp"
#include <cstdio>
#include <cstring>

TElement::TElement() {
    data = TComplex();
    p_prev = NULL;
    p_next = NULL;
}

TElement::TElement(TComplex _data) {
    data = _data;
    p_prev = NULL;
    p_next = NULL;
}

TElement::TElement(TComplex _data, TElement* _p_prev, TElement* _p_next) {
    data = _data;
    p_prev = _p_prev;
    p_next = _p_next;
}

void TElement::set_data(TComplex val) {
    data = val;
}

void TElement::set_p_prev(TElement* val) {
    p_prev = val;
}

void TElement::set_p_next(TElement* val) {
    p_next = val;
}

TComplex TElement::get_data() {
    return data;
}

TElement* TElement::get_p_prev() {
    return p_prev;
}

TElement* TElement::get_p_next() {
    return p_next;
}

void TElement::save_to_file(std::string file_name) {

    std::ofstream outfile;

    outfile.open(file_name, std::ios::app | std::ios::binary);
    outfile.seekp(std::ios_base::end);
    outfile << data << std::endl;
    outfile.close();
}

void TElement::load_from_file(std::string file_name, int pos_in_file) {

    std::ifstream infile;

    infile.open(file_name);
    std::string str;

    for (size_t i = 0; i < pos_in_file; i++) {
        std::getline(infile, str);
    }
    // Read the strings and convert them
    // to the float numbers using to_complex() func
    std::getline(infile, str);
    data = to_complex(str);

    infile.close();
}

void TElement::clear_file(std::string file_name) {

    std::ofstream outfile;
    outfile.open(file_name, std::ios_base::trunc);
}
