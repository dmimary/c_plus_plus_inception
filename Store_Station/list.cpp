﻿#include "list.hpp"

TList::TList() {
    file_name = "store.txt";
    p_begin = new TElement();
    p_end = p_begin;
}

TList::TList(TComplex _data) {
    file_name = "store.txt";
    p_begin = new TElement(_data);
    p_end = p_begin;
}

TList::~TList() {
    while (p_begin != NULL) {
        delete_element(p_begin);
    }
}

TElement* TList::get_p_begin() {
    return p_begin;
}

TElement* TList::get_p_end() {
    return p_end;
}

std::string TList::get_file_name() {
    return file_name;
}

void TList::add_element(TComplex _data) {
    TElement* new_elem = new TElement(_data);
    new_elem->set_p_prev(p_end);
    p_end->set_p_next(new_elem);
    p_end = new_elem;
}

void TList::add_element(TElement** elem) {
    (*elem)->set_p_prev(p_end);
    p_end->set_p_next(*elem);
    p_end = *elem;
}

TElement* TList::find_element(TComplex key) {

    TElement* p_current = p_begin;

    while(p_current != NULL && p_current->get_data() != key) {
        p_current = p_current->get_p_next();
    }

    return p_current;
}

TElement* TList::find_element(size_t key) {

    TElement* p_current = p_begin;
    size_t index = 1;

    while(p_current != NULL && index != key) {
        p_current = p_current->get_p_next();
        index++;
    }

    return p_current;
}

void TList::insert_elem_after(TElement* current, const TComplex& _data) {

    if (current == get_p_end()) {
        add_element(_data);
    } else {
        TElement* new_elem = new TElement(_data);
        new_elem->set_data(_data);
        new_elem->set_p_next(current->get_p_next());
        new_elem->set_p_prev( (current->get_p_next())->get_p_prev() );
        (current->get_p_next())->set_p_prev(new_elem);
        current->set_p_next(new_elem);
    }
}

void TList::delete_element(TElement* del_elem) {

    if (del_elem == p_begin) {
        p_begin = del_elem->get_p_next();  // = p_begin->get_p_next()
        if (p_begin != NULL)
            p_begin->set_p_prev(NULL);
    }
    else if (del_elem == p_end) {
        p_end = del_elem->get_p_prev();  // = p_end->get_p_next()
        p_end->set_p_next(NULL);
    } else {
        del_elem->get_p_prev()->set_p_next(del_elem->get_p_next());
    }

        delete del_elem;
}

bool TList::save_elements_to_file() {

    bool result = true;
    TElement* p_current = p_begin;

    try {
        while (p_current != NULL) {
            p_current->save_to_file(file_name);
            p_current = p_current->get_p_next();
        }
    } catch (const std::exception& ex) {
        result = false;
    }

    return result;
}

bool TList::load_elements_from_file() {

    bool result = true;
    TElement* elem = NULL;

    try {
        size_t qty_elems = get_file_elements_qty();
        for (int i = 0; i < qty_elems; i++) {
            elem = new TElement();
            elem->load_from_file(file_name, i);
            add_element(&elem);
        }
        // to delete the unnecessary elem from the top of the list
        delete_element(find_element(1));
    }
    catch (const std::exception& ex) {
        result = false;
    }

    return result;
}

size_t TList::get_file_elements_qty() {

    std::ifstream infile;
    infile.open(file_name);
    std::string temp;
    size_t counter = 0;

    if(!infile) {
        std::cerr << "ERROR: could not open file " << file_name << std::endl;
        std::terminate();
    }

    while (std::getline(infile, temp)) {
        ++counter;
    }

    return counter;
}

TComplex TList::get_sum() {

    TComplex sum = TComplex( p_begin->get_data().get_re(),
                             p_begin->get_data().get_im() );
    TElement* p_current = p_begin->get_p_next();

    while (p_current != NULL) {
        sum = sum + p_current->get_data();
        p_current = p_current->get_p_next();
    }

    return sum;
}

TComplex TList::get_difference() {

    TComplex diff = TComplex( p_begin->get_data().get_re(),
                              p_begin->get_data().get_im() );
    TElement* p_current = p_begin->get_p_next();

    while (p_current != NULL) {
        diff = diff - p_current->get_data();
        p_current = p_current->get_p_next();
    }

    return diff;
}

TComplex TList::get_product() {

    TComplex product = TComplex( p_begin->get_data().get_re(),
                                 p_begin->get_data().get_im() );
    TElement* p_current = p_begin->get_p_next();

    while (p_current != NULL) {
        product = product * p_current->get_data();
        p_current = p_current->get_p_next();
    }

    return product;
}

TComplex TList::get_quotient() {

    TComplex quot = TComplex( p_begin->get_data().get_re(),
                              p_begin->get_data().get_im() );
    TElement* p_current = p_begin->get_p_next();

    while (p_current != NULL) {
        quot = quot / p_current->get_data();
        p_current = p_current->get_p_next();
    }

    return quot;
}
