﻿#pragma once

#include <iostream>
#include <fstream>
#include <string>

struct TComplex {

    private:
        float Re;
        float Im;

    public:
        TComplex();
        TComplex(float _re, float _im);
        std::string to_string();

        TComplex operator + (const TComplex& _cn) const;
        TComplex operator - (const TComplex& _cn) const;
        TComplex operator * (const TComplex& _cn) const;
        TComplex operator / (const TComplex& _cn) const;
        bool operator == (const TComplex& _cn) const;
        bool operator != (const TComplex& _cn) const;

        void set_re(float val) { Re = val; }
        void set_im(float val) { Im = val; }

        float get_re() { return Re; }
        float get_im() { return Im; }

};

std::ostream& operator << (std::ostream& os, TComplex& _cn);
std::istream& operator >> (std::istream& is, TComplex& _cn);

TComplex to_complex(std::string _cn);
