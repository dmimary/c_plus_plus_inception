﻿#pragma once

#include "element.hpp"

struct TList {
// TODO: redo this struct od double-linked list to single-linked list
private:
    TElement* p_begin;
    TElement* p_end;

    std::string file_name;


public:
    TList();
    TList(TComplex _data);

    ~TList();

    TElement* get_p_begin();
    TElement* get_p_end();
    std::string get_file_name();

    void add_element(TComplex _data);
    void add_element(TElement** elem);
    TElement* find_element(TComplex key);  // find by data
    TElement* find_element(size_t key);  // find by series number
    void delete_element(TElement* del_elem);
    void insert_elem_after(TElement* current, const TComplex& _data);
    TComplex get_sum();
    TComplex get_difference();
    TComplex get_product();
    TComplex get_quotient();

    bool save_elements_to_file();
    bool load_elements_from_file();
    size_t get_file_elements_qty();

    void clear_list();
};
