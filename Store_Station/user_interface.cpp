﻿#include "user_interface.hpp"

#include <iostream>
#include <cstddef>  // size_t
#include <cstdlib>  // include exit, EXIT_FAILURE
#include <iomanip>  // setw

using std::cin;
using std::cout;
using std::endl;
using std::size_t;

static unsigned short int show_menu() {

    unsigned short int user_choice = 11;
    static const std::string separator = std::string(31, '~');

    cout << separator << endl
         << std::setw(27) << "Dynamic list operations" << endl
         << separator << endl
         << "0 - Add elements." << endl
         << "1 - Show list." << endl
         << "2 - Insert a new element after the existing one." << endl
         << "3 - Delete item by serial number in the list." << endl
         << "4 - Delete item by value." << endl
         << "5 - Save list to a file." << endl
         << "6 - Load data-list from file." << endl
         << "7 - Get a sum of all complex numbers in the current list." << endl
         << "8 - Get a sum of all saved complex numbers from the file." << endl
         << "9 - Get a difference of all complex numbers in the current list." << endl
         << "10 - Get a difference of all saved complex numbers from the file." << endl
         << "11 - Get a product of all complex numbers in a list." << endl
         << "12 - Get a product of all saved complex numbers from the file." << endl
         << "13 - Get a quotient of all complex numbers in a list." << endl
         << "14 - Get a quotient of all saved complex numbers from the file." << endl
         << "15 - Clear the file." << endl
         << "16 - Clear the current list." << endl
         << "17 - Exit." << endl;

    cin >> user_choice;

    return user_choice;
}

void show_interface() {
    TList* _list = NULL;
    unsigned short int user_choice = 0;

    do {
        user_choice = show_menu();

        switch (user_choice) {
        case 0:
            cout << endl;
            add_new_list_items(&_list);
            break;
        case 1:
            cout << endl;
            // TODO: don't break the func after the first calling this case!
            show_list_items(_list);
            cout << endl;
            break;
        case 2:
            cout << endl;
            insert_new_list_item(&_list);
            break;
        case 3:
            cout << endl;
            delete_list_item_by_idx(_list);
            break;
        case 4:
            cout << endl;
            delete_list_item_by_value(_list);
            break;
        case 5:
            cout << endl;
            save_list_items(_list);
            cout << endl;
            break;
        case 6:
            cout << endl;
            load_list_items_from_file(&_list);
            break;
        case 7:
            cout << endl;
            show_sum_total(_list);
            break;
        case 8:
            cout << endl;
            show_sum_total();
            break;
        case 9:
            cout << endl;
            show_difference_total(_list);
            break;
        case 10:
            cout << endl;
            show_difference_total();
            break;
        case 11:
            cout << endl;
            show_product_total(_list);
            break;
        case 12:
            cout << endl;
            show_product_total();
            break;
        case 13:
            cout << endl;
            show_quotient_total(_list);
            break;
        case 14:
            cout << endl;
            show_quotient_total();
            break;
        case 15:
            cout << endl;
            clear_the_file(_list);
            break;
        case 16:
            cout << endl;
            clear_the_list(&_list);
            break;
        case 17:
            cout << endl;
            cout << "Exit the menu. So long =^.^=" << endl << endl;
            break;
        default:
            std::cerr << "ERROR: Wrong menu value! Please, try again." << endl;
        }
    } while(user_choice != 17);

    delete _list;
}

static void add_new_list_items(TList** _list) {

    TComplex c_num;
    size_t count = 0;

    cout << "Please, enter a total number of the new items: ";
    cin >> count;

    for (int i = 0; i < count; i++) {
        cout << "Please, enter a complex number " << i << ": ";
        cin >> c_num;
        if ( *_list == NULL )
            *_list = new TList(c_num);
        else
            (*_list)->add_element(c_num);
    }
}

static void show_list_items(TList* _list) {

    size_t counter = 0;
    TComplex data;
    TElement* p_current = _list->get_p_begin();

    cout << "User list items: " << endl;

    while (p_current != NULL) {
        counter++;
        data = p_current->get_data();
        cout << counter << ". " << data << endl;
        p_current = p_current->get_p_next();
    }
}

static void insert_new_list_item(TList** _list) {
    TComplex c_num;
    size_t index = 0;

    cout << "Please, enter a serial number of item after which a new one will be"
            " inserted" << endl << "(start from 1): ";
    cin >> index;

//  analogy with delete fist of all to find a pointer by key - serial number
    TElement* p_current = (*_list)->find_element(index);
    if (p_current == NULL) {
        std::cerr << "Given serial number was not found! Please, try again." << endl;
     } else {
        cout << "Please, enter a new complex number: ";
        cin >> c_num;
        cout << endl;
        (*_list)->insert_elem_after(p_current, c_num);
    }
}

static void delete_list_item_by_idx(TList* _list) {
// TODO: add feature to delete elems in a range
// NOTE: if user doesn't input the second index therefor delete only one numbers otherwise if second index is less then the first delete in anyways
    size_t s_num;

    cout << "Please, enter a serial number to delete (the first is 1): ";
    cin >> s_num;
    cout << endl;

    TElement* p_current = _list->find_element(s_num);
    if (p_current == NULL)
        std::cerr << "Given serial number is wrong! Please, try again."
                  << endl << endl;
    else
        _list->delete_element(p_current);
}

static void delete_list_item_by_value(TList* _list) {

    TComplex c_num;

    cout << "Please, enter a complex number to delete: ";
    cin >> c_num;
    cout << endl;

    TElement* p_current = _list->find_element(c_num);
    if (p_current == NULL)
        std::cerr << "Given element was not found! Please, try again."
                  << endl << endl;
    else
        _list->delete_element(p_current);
}

static void save_list_items(TList* _list) {

    if ( _list->save_elements_to_file() )
        cout << "Saving the user list to store.txt file was successful!" << endl;
    else
        cout << "Saving the user list to store.txt file was UNsuccessful! "
                "Please, try later." << endl;
}

static void load_list_items_from_file(TList** _list) {

    if (*_list == NULL)
        *_list = new TList();

    if ( (*_list)->load_elements_from_file() ) {
        cout << "Loading of list from file was successful!" << endl << endl;
//        (*_list)->delete_element((*_list)->find_element(1));  // to delete the unnecessary elem from the top of the list
    } else {
        cout << "Loading of list from file was UNsuccessful! Please, try later."
             << endl << endl;
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~Binary Arithmetic Opeartions~~~~~~~~~~~~~~~~~~~~~~~~

// Sum of elements from the file
static void show_sum_total() {

    TList* _list = new TList;

    if (_list->load_elements_from_file()) {

        if (_list->get_p_begin()->get_p_next() == NULL) {
            std::cerr << "Fill in the list with at least 2 complex numbers, "
                         "please!";
            return;
        }

        TComplex sum = _list->get_sum();

        cout << "A sum total of saved elements from the file:" << endl
             << "*** SUM = " << sum << endl << endl;
    } else {
        cout << "Loading of list from file was UNsuccessful!" << endl
             << "Please, make sure that the file is not empty." << endl;
    }
}

// Sum of recently entered numbers in a user list
static void show_sum_total(TList* _list) {

    if (_list->get_p_begin()->get_p_next() == NULL) {
        std::cerr << "Fill in the list with at least 2 complex numbers, "
                     "please!";
        return;
    }

    TComplex sum = _list->get_sum();

    cout << "A sum total of elements in the current list: " << endl
         << "*** SUM = " << sum << endl << endl;
}

static void show_difference_total() {

    TList* _list = new TList;

    if (_list->load_elements_from_file()) {

        if (_list->get_p_begin()->get_p_next() == NULL) {
            std::cerr << "Fill in the list with at least 2 complex numbers, "
                         "please!";
            return;
        }

        TComplex diff = _list->get_difference();

        cout << "A total difference of saved elements from the file:" << endl
             << "*** DIFFERENCE = " << diff << endl << endl;
    } else {
        cout << "Loading of list from file was UNsuccessful!" << endl
             << "Please, make sure that the file is not empty." << endl;
    }
}

static void show_difference_total(TList* _list) {

    if (_list->get_p_begin()->get_p_next() == NULL) {
        std::cerr << "Fill in the list with at least 2 complex numbers, "
                     "please!";
        return;
    }

    TComplex diff = _list->get_difference();

    cout << "A total difference of elements in the current list: " << endl
         << "*** DIFFERENCE = " << diff << endl << endl;
}

static void show_product_total() {

    TList* _list = new TList;

    if (_list->load_elements_from_file()) {

        if (_list->get_p_begin()->get_p_next() == NULL) {
            std::cerr << "Fill in the list with at least 2 complex numbers, "
                         "please!";
            return;
        }

        TComplex product = _list->get_product();

        cout << "A total product of saved elements from the file:" << endl
             << "*** PRODUCT = " << product << endl << endl;
    } else {
        cout << "Loading of list from file was UNsuccessful!" << endl
             << "Please, make sure that the file is not empty." << endl;
    }
}

static void show_product_total(TList* _list) {

    if (_list->get_p_begin()->get_p_next() == NULL) {
        std::cerr << "Fill in the list with at least 2 complex numbers, "
                     "please!";
        return;
    }

    TComplex product = _list->get_product();

    cout << "A product total of elements in the current list: " << endl
         << "*** PRODUCT = " << product << endl << endl;
}

static void show_quotient_total() {

    TList* _list = new TList;

    if (_list->load_elements_from_file()) {

        if (_list->get_p_begin()->get_p_next() == NULL) {
            std::cerr << "Fill in the list with at least 2 complex numbers, "
                         "please!";
            return;
        }

        TComplex quot = _list->get_quotient();

        cout << "A total quotient of saved elements from the file:" << endl
             << "*** QUOTIENT = " << quot << endl << endl;
    } else {
        cout << "Loading of list from file was UNsuccessful!" << endl
             << "Please, make sure that the file is not empty." << endl;
    }
}

static void show_quotient_total(TList* _list) {

    if (_list->get_p_begin()->get_p_next() == NULL) {
        std::cerr << "Fill in the list with at least 2 complex numbers, "
                     "please!";
        return;
    }

    TComplex quot = _list->get_quotient();

    cout << "A total quotient of elements in the current list: " << endl
         << "*** QUOTIENT = " << quot << endl << endl;
}

static void clear_the_file(TList* _list) {

    _list->get_p_begin()->clear_file(_list->get_file_name());
    cout << "File is cleared!" << endl << endl;
}

static void clear_the_list(TList** _list) {

    (*_list)->~TList();
    (*_list) = NULL;
    cout << "Current list is cleared!" << endl << endl;
}
