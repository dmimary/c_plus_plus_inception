﻿#pragma once

#include "list.hpp"

static unsigned short int show_menu();

void show_interface();

static void add_new_list_items(TList** _list);

static void show_list_items(TList* _list);

static void insert_new_list_item(TList** _list);

static void delete_list_item_by_idx(TList* _list);

static void delete_list_item_by_value(TList* _list);

static void save_list_items(TList* _list);

static void load_list_items_from_file(TList** _list);

static void show_sum_total();

static void show_sum_total(TList* _list);

static void show_difference_total();

static void show_difference_total(TList* _list);

static void show_product_total();

static void show_product_total(TList* _list);

static void show_quotient_total();

static void show_quotient_total(TList* _list);

static void clear_the_file(TList* _list);

static void clear_the_list(TList** _list);
