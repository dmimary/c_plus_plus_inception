﻿#include "complex.hpp"

#include <cmath>

using std::pow;

TComplex::TComplex() {
    Re = 0.0;
    Im = 0.0;
}

TComplex::TComplex(float _re, float _im) {
    Re = _re;
    Im = _im;
}

std::string TComplex::to_string() {
    return std::to_string(Re) + (Im < 0 ? " - " : " + ") +
                                std::to_string(Im < 0 ? Im * -1 : Im) + 'i';
}

TComplex TComplex::operator + (const TComplex& _cn) const {
    return TComplex(Re + _cn.Re, Im + _cn.Im);
}

TComplex TComplex::operator - (const TComplex& _cn) const {
    return TComplex(Re - _cn.Re, Im - _cn.Im);
}

TComplex TComplex::operator * (const TComplex& _cn) const {
    return TComplex(Re * _cn.Re - Im * _cn.Im,
                    Re * _cn.Im + _cn.Re * Im);
}

TComplex TComplex::operator / (const TComplex& _cn) const {
    return TComplex( (Re * _cn.Re + Im * _cn.Im) /
             (pow(_cn.Re, 2.0) + pow(_cn.Im, 2.0)),
             (_cn.Re * Im + Re * _cn.Im) /
             (pow(_cn.Re, 2.0) + pow(_cn.Im, 2.0)) );
}

bool TComplex::operator == (const TComplex& _cn) const {
    return (Re == _cn.Re) && (Im == _cn.Im);
}

bool TComplex::operator != (const TComplex& _cn) const {
    return (Re != _cn.Re) || (Im != _cn.Im);
}

std::ostream& operator << (std::ostream& os, TComplex& _cn) {

    os << _cn.to_string();
    return os;
}

std::istream& operator >> (std::istream& is, TComplex& _cn) {
    float val = 0.0;

    is >> val;
    _cn.set_re(val);

    is >> val;
    _cn.set_im(val);

    return is;
}

TComplex to_complex(std::string _cn) {

    std::string::size_type sz;     // alias of size_t
    float re = std::stof(_cn, &sz);
    float im = std::stof(_cn.substr(sz + 3));

    TComplex new_cn = TComplex(re, im);

    return new_cn;
}
