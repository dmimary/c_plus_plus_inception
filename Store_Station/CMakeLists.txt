﻿cmake_minimum_required(VERSION 2.8)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

project(Store_Station)
add_executable(${PROJECT_NAME}
    "main.cpp"
    "complex.hpp"
    "complex.cpp"
    "element.hpp"
    "element.cpp"
    "list.hpp"
    "list.cpp"
    "user_interface.hpp"
    "user_interface.cpp")
