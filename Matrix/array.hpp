﻿#pragma once

#include <iostream>
#include <cstddef> // size_t

class TArray {
    // data members
    int* array;
    size_t size;

// access specifier
public:
    // constructor
    TArray(size_t size);

    //destructor
    ~TArray();

    // member functions:
    //! Overload [] array subscript operator.
    int& operator [] (std::size_t idx);

    //! Overload << ostream to display array.
    friend std::ostream& operator << (std::ostream& os, const TArray& arr);
};
