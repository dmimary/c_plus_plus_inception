﻿#pragma once

#include "array.hpp"

using std::size_t;

class TMatrix {

    int** matrix;
    size_t rows;
    size_t columns;
//access specifier
public:
    // constructor
    TMatrix(size_t rows, size_t columns);

    // destructor
    ~TMatrix();

    // member functions:
    //! Initialize a matrix by random integers [99,-99] for using in other funcs.
    void init_matrix();

    //! Initialize array of sums of matrix rows elements.
    void sum(TArray& arr);  // reference to a array class object

    //! Initialize array of product of matrix rows elements.
    void product(TArray& arr);

    //! Initialize array of average (mean value) of matrix rows elements.
    void average(TArray& arr);

    //! Overload << ostream to display matrix.
    friend std::ostream& operator << (std::ostream& os, const TMatrix& arr2D);
};
