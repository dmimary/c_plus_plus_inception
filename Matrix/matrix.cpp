﻿#include "matrix.hpp"

#include <cstdlib>  // rand()
#include <iomanip>  // setw

using std::size_t;

TMatrix::TMatrix(size_t rows, size_t columns) : rows{rows}, columns{columns} {
    matrix = new int*[rows];
    for (size_t i = 0; i < rows; i++)
        matrix[i] = new int[columns];  // *(matrix + i)
}

TMatrix::~TMatrix() {

    // Deallocate obtained memory area
        for(size_t i = 0; i < rows; i++)
            delete[] matrix[i];
}

void TMatrix::init_matrix() {

    const unsigned short int CONST_198 = 198;
    const unsigned short int CONST_99 = 99;

    // initialize matrix by random numbers [-99;99]
    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < columns; j++) {
            matrix[i][j] = rand() % CONST_198 - CONST_99;
        }
}

// Display a matrix.
std::ostream& operator << (std::ostream& os, const TMatrix& arr2D) {

    for (size_t i = 0; i < arr2D.rows; i++) {
        os << std::endl << std::setw(2) << i << '|';
            for (size_t j = 0; j < arr2D.columns; j++)
                os << std::setw(3) << arr2D.matrix[i][j] << ' ';
    }

    return os;
}

void TMatrix::sum(TArray& arr) {

    int sum = 0;

    for (size_t i = 0; i < rows; i++) {
        for (size_t j = 0; j < columns; j++) {
            sum += matrix[i][j];
        }
        arr[i] = sum;
        sum = 0;
    }
}

void TMatrix::product(TArray& arr) {

    int product = 1;

    for (size_t i = 0; i < rows; i++) {
        for (size_t j = 0; j < columns; j++) {
                product *= matrix[i][j];
            }
            arr[i] = product;
            product = 1;
        }
}

void TMatrix::average(TArray& arr) {

    int average = 0;

    for (size_t i = 0; i < rows; i++) {
        for (size_t j = 0; j < columns; j++) {
                average += matrix[i][j];
            }
            arr[i] = average / rows;
            average = 0;
        }
}
