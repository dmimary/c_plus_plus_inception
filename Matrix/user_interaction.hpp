﻿#pragma once

#include "matrix.hpp"

//! Display a matrix and array - result of array_of_rows_sums func.
void show_array_of_rows_sums();
