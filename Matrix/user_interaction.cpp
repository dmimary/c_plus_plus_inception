﻿#include "user_interaction.hpp"

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

void show_array_of_rows_sums() {

    size_t rows = 0;
    size_t columns = 0;

    cout << endl
         << "~~~ Task #2(a). Build array of sums of matrix rows elements. ~~~"
         << endl << endl
         << "Integer matrix will be initialized by integers in range [-99;99]."
         << endl
         << "Please, input matrix dimensions: number of rows and columns: ";
    cin >> rows >> columns;

    TMatrix matrix = TMatrix(rows, columns);

    matrix.init_matrix();

    cout << endl << "Matrix["  << rows << "][" << columns << "]:" << endl
         << matrix << endl << endl;

    TArray array_s = TArray(rows);
    TArray array_p = TArray(rows);
    TArray array_av = TArray(rows);

    matrix.sum(array_s);  //  NOTE: Why not matrix.sum(&array_s) to take address?
    matrix.product(array_p);
    matrix.average(array_av);

    cout << "1. array[" << rows << "] Each element equals a sum of"
                    " matrix rows elements:" << endl << endl << array_s << endl
         << "2. array[" << rows << "] Each element equals a product of matrix "
                                   "rows elements:" << endl << endl << array_p
         << endl
         << "3. array[" << rows << "] Each element equals a product of average "
                                   "(mean value) of matrix rows elements:"
         << endl << endl << array_av;

    cout << endl << "THE END =^.^=" << endl;

}
