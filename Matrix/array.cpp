﻿#include "array.hpp"

TArray::TArray(size_t size) : size{size} {

    array = new int[size];
}

// Deallocate obtained memory area
TArray::~TArray() {
    delete[] array;
}

int& TArray::operator [] (std::size_t idx) {
    return array[idx];
}

std::ostream& operator << (std::ostream& os, const TArray& arr) {

    for (size_t i = 0; i < arr.size; i++) {
        os << "array[" << i << "] = " << arr.array[i] << std::endl;
    }

    return os;
}
