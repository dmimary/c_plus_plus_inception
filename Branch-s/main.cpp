﻿/*!
  Set of different examples to study IF...ELSE statement.

  @author Maria Dmitrieva
*/

#include <iostream>
#include <cassert>

#include "branch-s.hpp"

using std::cout;
using std::endl;

int main()
{
    // Test ComputeFx func, task #5.
    assert(ComputeFx(-3.5) == 3.5);
    assert(ComputeFx(1.5) == 3);
    assert(ComputeFx(5) == 25);
    cout << "Task #5: Tests are passed.\n" << endl;

    // Call IsThereTriangle func task #7
    cout << "Task #7:" << endl;
    IsItTriangle(7, 7, 7);
    IsItTriangle(5, 4, 3);
    IsItTriangle(12, 3.5, 6.5);
    cout << endl;

    // Test DetermineYbyX func, task #11
    assert(DetermineYbyX(2) == 4);
    assert(DetermineYbyX(1) == 1);
    assert(DetermineYbyX(-1) == 1);
    assert(DetermineYbyX(-2) == 0.25);
    cout << "Task #11: Tests are passed.\n" << endl;

    // Test ChangeSign func, task #21
    int x = 5, y = 5, z = 5;
    ChangeSign(x, y, z);  // 5, 5, 5
    assert(x == -5);
    ChangeSign(x, y, z);  // -5, 5, 5
    assert(y == -5);
    ChangeSign(x, y, z);  // -5, -5, 5
    assert(z == -5);
    x += 1;
    y += 1;
    ChangeSign(x, y, z);  // -4, -4, -5
    assert(x == 4);
    y *= -1;
    z *= -1;
    ChangeSign(x, y, z);  // 4, 4, 5
    assert(z == -5);
    cout << "Task #21: Tests are passed.\n" << endl;

    // Call RefuelCar func, task #22
    cout << "Task #22:\n" << "1) price: 30; money: 150; liters: 5\n";
    RefuelCar(30, 150, 5);
    cout << "2) price: 30; money: 200; liters: 5\n";
    RefuelCar(30, 200, 5);
    cout << "3) price: 30; money: 100; liters: 5\n";
    RefuelCar(30, 100, 5);
    cout << "4) price: 25.9; money: 100; liters: 5\n";
    RefuelCar(25.9, 150, 5.5);
    cout << "4) price: 38.5; money: 100; liters: 5\n";
    RefuelCar(38.5, 300, 8.0);

    return 0;
}
