﻿#pragma once

//! H/w 1. Branching, task #5. Calculate F(x), x - real.
float ComputeFx(float x);

/*!
 * H/w 1. Branching, task #7.
 * Determine whether there is a triangle with x, y, z lenghts of sides and
 * output the message.
*/
void IsItTriangle(float x, float y, float z);

//! H/w 1. Branching, task #11. For the function graphs, determine y by given x.
float DetermineYbyX(float x);

/*!
 * H/w 1. Branching, task #21.
 * Change the sign of max number of 3,
 * if it’s greater than half the sum all of them.
*/
void ChangeSign(int& x, int& y, int& z);

/*!
 * H/w 1. Branching, task #22.
 * Car refueling. Depending on gasoline price, amount of money and number of
 * liters determine whether there is enough money or not or retern the change.
*/
void RefuelCar(float price, float money, float liters);
