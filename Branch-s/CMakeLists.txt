﻿cmake_minimum_required(VERSION 2.8)

project(Branch-s)
add_executable(${PROJECT_NAME}
    "main.cpp"
    "branch-s.cpp"
    "branch-s.hpp")
