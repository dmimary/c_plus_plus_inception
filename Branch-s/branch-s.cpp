﻿#include <iostream>
#include <stdio.h>  // to use printf() in last RefuelCar func

#include "branch-s.hpp"

using std::cout;
using std::endl;

// H/w 1. Branching, task #5.
// Calculate F(x), x - real.
float ComputeFx(float x) {
    float Fx = 0.0;

    if (x < 2.0) {
        if (x < 0.0) {
            Fx = -1 * x;
        } else {
            Fx = 2 * x;
        }
    } else {
        Fx = x * x;
    }

    return Fx;
}

/*
 * H/w 1. Branching, task #7.
 * Determine whether there is a triangle with x, y, z lenghts of sides and
 * output the message.
*/
void IsItTriangle(float x, float y, float z) {
    bool conditional = (x + y > z) && (x + z > y) && (y + z > x);

    if (conditional) {
        cout << "Triangle with " << x << " " << y << " " << z
             << " sides exists." << endl;
    } else {
        cout << "Triangle with " << x << " " << y << " " << z
             << " sides doesn't exist." << endl;
    }
}

// H/w 1. Branching, task #11.
// For the function graphs, determine y by given x.
float DetermineYbyX(float x) {
    float y = 0.0;
    if (x < 2.0) {
        if (x < -1.0) {
            y = 1 / (x * x);
        } else {
            y = x * x;
        }
    } else {
        y = 4.0;
    }

    return y;
}

// H/w 1. Branching, task #21.
// Change the sign of max number of 3,
// if it’s greater than half the sum all of them.
void ChangeSign(int& x, int& y, int& z) {
    bool conditional = x >= (y + z) / 2.0;

    if (conditional) {
        x *= -1;
    } else {
        conditional = y > (x + z) / 2.0;
        if (conditional) {
            y *= -1;
        } else {
            conditional = z > (x + y) / 2.0;
            if (conditional) {
                z *= -1;
            }
        }
    }
}

// H/w 1. Branching, task #22.
// Car refueling. Depending on gasoline price, amount of money and number of
// liters determine whether there is enough money or not or retern the change.
void RefuelCar(float price, float money, float liters){
    float enoughL = money / price;    // Money is enough for "enoughL" liters.
    if (enoughL == liters) {
        cout << "-- Car is refueled. Thanks!\n" << endl;
    }
    else if (enoughL > liters) {
        float change = money - liters * price;
        cout << "-- Here's your change: " << change << endl << endl;
    }
    else {
        float not_enough = liters * price - money;
        cout << "-- Hey, there is not enough money!\n"
                "-- Still need " << not_enough << " money.\n";
        std::printf("-- Or it’s enough for %4.2f liters.\n\n", enoughL);
    }
}
