﻿#include <iostream>
#include "stack.hpp"

using std::cout;
using std::cin;
using std::endl;

void create(TNode** top, int d) {
    *top = new TNode;

    (*top)->data = d;
    (*top)->ptr = NULL;
}

void push(TNode** top, int d) {
    TNode* p_value = new TNode; // p_value -pointer value
    if (*top == NULL) {
        create(top, d);
    }
    else {
        p_value->data = d;
        p_value->ptr = *top;  // node new elem with top
        *top = p_value;  // move pointer of the top of stack
    }
}

int pop(TNode** top) {
    int temp = (*top)->data; // () higher precedence
    TNode* p_value = *top;
    *top = (*top)->ptr;
    delete p_value;

    return temp;
}

void init_stack(TNode** top) {
    int elem_value;

    for (int i = 0; i < STACK_QTY; i++) {
        cout << "The " << i << "elem in stack = ";
        cin >> elem_value;
        push(top, elem_value);
        cout << endl;
    }
}

/*
void display_stack(TNode** top) {
    while (top != NULL) {

    }
}
*/
