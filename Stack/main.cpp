﻿#include <iostream>
#include "stack.hpp"

using std::cout;
using std::endl;

int main()
{
    TNode** _top = NULL;
    _top = new TNode*;
    int _data = 99;

    create(_top,_data);

    _data = -99;

    push(_top, _data);

    cout << "Stack contains: 99 -99 "<< endl;

    cout << "Top elem " << pop(_top) << " was popped." << endl;

    cout << "Stack contains: " << (*_top)->data << endl;

    cout << "Top elem " << pop(_top) << " was popped." << endl;

    return 0;
}
