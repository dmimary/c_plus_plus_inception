﻿#pragma once

const unsigned int STACK_QTY = 10;

struct TNode {
    int data;
    TNode* ptr;
};

//! Create stack
void create(TNode** top, int d);

//! Push new elem in stack top
void push(TNode** top, int d);

//! Pop the top elem from stack
int pop(TNode** top);

void init_stack(TNode** top);
